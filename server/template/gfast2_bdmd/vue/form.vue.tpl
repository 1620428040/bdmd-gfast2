{{$hasRichText:=false}}
{{$hasTreeSelect:=false}}
{{$hasImageFile:=false}}
{{$hasDuration:=false}}
{{$linkUser:=false}}
{{range $index,$column:=.table.Columns}}
    {{if eq $column.HtmlType "richtext"}}{{$hasRichText = true}}{{end}}
    {{if eq $column.HtmlType "treeselect"}}{{$hasTreeSelect = true}}{{end}}
    {{if eq $column.HtmlType "imagefile"}}{{$hasImageFile = true}}{{end}}
    {{if eq $column.HtmlType "duration"}}{{$hasDuration = true}}{{end}}
    {{if in $column.Special "current_user" "current_dept"}}{{$linkUser = true}}{{end}}
{{end}}

<template>
    <div class="app-container">
        <el-form ref="form" :model="form" :rules="rules" label-width="80px">
            {{range $index, $column := .table.Columns}}
            {{  if and (eq $column.IsInsert "1") (ne $column.IsPk "1") (ne $column.HtmlType "underlying")}}
            {{      if eq $column.HtmlType "input"}}
            <el-form-item label="{{$column.ColumnComment}}" prop="{{$column.HtmlField}}" {{if $column.IsShow}}v-show="{{$column.IsShow|JsExpr "form."}}" {{end}}>
                <el-input v-model="form.{{$column.HtmlField}}" placeholder="请输入{{$column.ColumnComment}}" {{if eq $column.Immutable "1"}}:disabled="true" {{end}} />
            </el-form-item>

            {{      else if eq $column.HtmlType "number" }}
            <el-form-item label="{{$column.ColumnComment}}" prop="{{$column.HtmlField}}" {{if $column.IsShow}}v-show="{{$column.IsShow|JsExpr "form."}}" {{end}}>
                <el-input-number v-model="form.{{$column.HtmlField}}" {{if eq $column.Immutable "1"}}:disabled="true" {{end}}></el-input-number>
            </el-form-item>

            {{      else if eq $column.HtmlType "code"}}
            <el-form-item label="{{$column.ColumnComment}}" prop="{{$column.HtmlField}}" {{if $column.IsShow}}v-show="{{$column.IsShow|JsExpr "form."}}" {{end}}>
                {{if ne $column.Immutable "1"}}
                <el-input v-model="form.{{$column.HtmlField}}" placeholder="请输入{{$column.ColumnComment}}或点击自动生成">
                    <el-button slot="append" @click="genCode">自动生成</el-button>
                </el-input>
                {{else}}
                <el-input v-model="form.{{$column.HtmlField}}" placeholder="请输入{{$column.ColumnComment}}或点击自动生成" :disabled="true"/>
                {{end}}
            </el-form-item>

            {{      else if eq $column.HtmlType "select" }}
            {{          if ne $column.LinkTableName ""}}
            <el-form-item label="{{$column.ColumnComment}}" prop="{{$column.HtmlField}}" {{if $column.IsShow}}v-show="{{$column.IsShow|JsExpr "form."}}" {{end}}>
                <el-select v-model="form.{{$column.HtmlField}}" placeholder="请选择{{$column.ColumnComment}}" {{if eq $column.Immutable "1"}}:disabled="true" {{end}}>
                    <el-option
                            v-for="item in {{$column.HtmlField}}Options"
                            :key="item.key"
                            :label="item.value"
                            :value="item.key"
                    ></el-option>
                </el-select>
            </el-form-item>

            {{          else if ne $column.DictType ""}}
            <el-form-item label="{{$column.ColumnComment}}" prop="{{$column.HtmlField}}" {{if $column.IsShow}}v-show="{{$column.IsShow|JsExpr "form."}}" {{end}}>
                <el-select v-model="form.{{$column.HtmlField}}" placeholder="请选择{{$column.ColumnComment}}" {{if eq $column.Immutable "1"}}:disabled="true" {{end}}>
                    <el-option
                            v-for="dict in {{$column.HtmlField}}Options"
                            :key="dict.key"
                            :label="dict.value"
                            {{if eq $column.GoType "Integer"}}
                            :value="parseInt(dict.key)"
                            {{else}}
                            :value="dict.key"
                            {{end}}
                    ></el-option>
                </el-select>
            </el-form-item>
            {{          else}}
            <el-form-item label="{{$column.ColumnComment}}" prop="{{$column.HtmlField}}" {{if $column.IsShow}}v-show="{{$column.IsShow|JsExpr "form."}}" {{end}}>
                <el-select v-model="form.{{$column.HtmlField}}" placeholder="请选择{{$column.ColumnComment}}" {{if eq $column.Immutable "1"}}:disabled="true" {{end}}>
                    <el-option label="请选择字典生成" value="" />
                </el-select>
            </el-form-item>
            {{          end}}

            {{      else if eq $column.HtmlType "refer" }}
            <el-form-item label="{{$column.ColumnComment}}" prop="{{$column.HtmlField}}" {{if $column.IsShow}}v-show="{{$column.IsShow|JsExpr "form."}}" {{end}}>
                {{if eq $column.Immutable "1"}}
                <el-input v-model="{{$column.LabelField|CaseCamelLower}}" placeholder="请选择{{$column.ColumnComment}}" :readonly="true" :disabled="true"></el-input>
                {{else}}
                <el-input v-model="{{$column.LabelField|CaseCamelLower}}" placeholder="请选择{{$column.ColumnComment}}" :readonly="true" >
                    <el-button slot="append" icon="el-icon-search" @click="{{$column.HtmlField}}Open=true"></el-button>
                </el-input>
                <el-dialog title="从已有{{$column.ColumnComment}}中选择或新增{{$column.ColumnComment}}" :visible.sync="{{$column.HtmlField}}Open" width="800px" append-to-body :close-on-click-modal="false">
                    <{{$column.HtmlField}}Refer ref="{{$column.HtmlField}}Refer"
                        {{range $to, $from := $column.LinkFilter}}
                        :{{$to|CaseCamelLower}} = "form.{{$from|CaseCamelLower}}"
                        {{end}}
                    ></{{$column.HtmlField}}Refer>
                    <div slot="footer" class="dialog-footer">
                        <el-button type="primary" @click="{{$column.HtmlField}}Confirm">确 定</el-button>
                        <el-button @click="{{$column.HtmlField}}Cancel">取 消</el-button>
                    </div>
                </el-dialog>
                {{end}}
            </el-form-item>

            {{      else if eq $column.HtmlType "treeselect" }}
            <el-form-item label="{{$column.ColumnComment}}" prop="{{$column.HtmlField}}" {{if $column.IsShow}}v-show="{{$column.IsShow|JsExpr "form."}}" {{end}}>
                <treeselect v-model="form.{{$column.HtmlField}}" :options="{{$column.HtmlField}}Tree" :normalizer="normalizer" placeholder="请选择所属分类" {{if eq $column.Immutable "1"}}:disabled="true" {{end}}/>
            </el-form-item>

            {{      else if eq $column.HtmlType "radio" }}
            {{          if ne $column.DictType ""}}
            <el-form-item label="{{$column.ColumnComment}}" prop="{{$column.HtmlField}}" {{if $column.IsShow}}v-show="{{$column.IsShow|JsExpr "form."}}" {{end}}>
                <el-radio-group v-model="form.{{$column.HtmlField}}" {{if eq $column.Immutable "1"}}:disabled="true" {{end}}>
                    <el-radio
                            v-for="dict in {{$column.HtmlField}}Options"
                            :key="dict.key"
                            :label="dict.key"
                    >{{ VueTag "{{" }}dict.value {{VueTag "}}"}}</el-radio>
                </el-radio-group>
            </el-form-item>
            {{          else}}
            <el-form-item label="{{$column.ColumnComment}}" prop="{{$column.HtmlField}}" {{if $column.IsShow}}v-show="{{$column.IsShow|JsExpr "form."}}" {{end}}>
                <el-radio-group v-model="form.{{$column.HtmlField}}" {{if eq $column.Immutable "1"}}:disabled="true" {{end}}>
                    <el-radio label="1">请选择字典生成</el-radio>
                </el-radio-group>
            </el-form-item>
            {{          end}}

            {{      else if eq $column.HtmlType "date"}}
            <el-form-item label="{{$column.ColumnComment}}" prop="{{$column.HtmlField}}" {{if $column.IsShow}}v-show="{{$column.IsShow|JsExpr "form."}}" {{end}}>
                <el-date-picker clearable size="small" style="width: 200px"
                                v-model="form.{{$column.HtmlField}}"
                                type="date"
                                value-format="yyyy-MM-dd HH:mm:ss"
                                placeholder="选择{{$column.ColumnComment}}" {{if eq $column.Immutable "1"}}:disabled="true" {{end}}>
                </el-date-picker>
            </el-form-item>

            {{      else if eq $column.HtmlType "time"}}
            <el-form-item label="{{$column.ColumnComment}}" prop="{{$column.HtmlField}}" {{if $column.IsShow}}v-show="{{$column.IsShow|JsExpr "form."}}" {{end}}>
                <el-date-picker clearable size="small" style="width: 200px"
                                v-model="form.{{$column.HtmlField}}"
                                type="time"
                                value-format="yyyy-MM-dd HH:mm:ss"
                                placeholder="选择{{$column.ColumnComment}}" {{if eq $column.Immutable "1"}}:disabled="true" {{end}}>
                </el-date-picker>
            </el-form-item>

            {{      else if eq $column.HtmlType "datetime"}}
            <el-form-item label="{{$column.ColumnComment}}" prop="{{$column.HtmlField}}" {{if $column.IsShow}}v-show="{{$column.IsShow|JsExpr "form."}}" {{end}}>
                <el-date-picker clearable size="small" style="width: 200px"
                                v-model="form.{{$column.HtmlField}}"
                                type="datetime"
                                value-format="yyyy-MM-dd HH:mm:ss"
                                placeholder="选择{{$column.ColumnComment}}" {{if eq $column.Immutable "1"}}:disabled="true" {{end}}>
                </el-date-picker>
            </el-form-item>

            {{      else if eq $column.HtmlType "duration"}}
            <el-form-item label="{{$column.ColumnComment}}" prop="{{$column.HtmlField}}" {{if $column.IsShow}}v-show="{{$column.IsShow|JsExpr "form."}}" {{end}}>
                <div class="duration">
                    <el-input v-model="{{$column.HtmlField}}.hour" @change="{{$column.HtmlField}}Change" type="number" :min="0" {{if eq $column.Immutable "1"}}:disabled="true" {{end}} >
                        <template slot="append">小时</template>
                    </el-input>
                    <el-input v-model="{{$column.HtmlField}}.minute" @change="{{$column.HtmlField}}Change" type="number" :min="0" {{if eq $column.Immutable "1"}}:disabled="true" {{end}} >
                        <template slot="append">分钟</template>
                    </el-input>
                    <el-input v-model="{{$column.HtmlField}}.second" @change="{{$column.HtmlField}}Change" type="number" :min="0" {{if eq $column.Immutable "1"}}:disabled="true" {{end}} >
                        <template slot="append">秒</template>
                    </el-input>
                </div>
            </el-form-item>

            {{      else if eq $column.HtmlType "textarea"}}
            <el-form-item label="{{$column.ColumnComment}}" prop="{{$column.HtmlField}}" {{if $column.IsShow}}v-show="{{$column.IsShow|JsExpr "form."}}" {{end}}>
                <el-input v-model="form.{{$column.HtmlField}}" type="textarea" placeholder="请输入{{$column.ColumnComment}}" {{if eq $column.Immutable "1"}}:disabled="true" {{end}} />
            </el-form-item>

            {{      else if eq $column.HtmlType "checkbox" }}
            <el-form-item label="{{$column.ColumnComment}}" prop="{{$column.HtmlField}}" {{if $column.IsShow}}v-show="{{$column.IsShow|JsExpr "form."}}" {{end}}>
                <el-checkbox-group v-model="form.{{$column.HtmlField}}" {{if eq $column.Immutable "1"}}:disabled="true" {{end}}>
                    <el-checkbox
                            v-for="dict in {{$column.HtmlField}}Options"
                            :key="dict.key"
                            :label="dict.key"
                    >{{ VueTag "{{" }}dict.value {{VueTag "}}"}}</el-checkbox>
                </el-checkbox-group>
            </el-form-item>

            {{      else if eq $column.HtmlType "imagefile"}}
            <el-form-item label="{{$column.ColumnComment}}" prop="{{$column.HtmlField}}" {{if $column.IsShow}}v-show="{{$column.IsShow|JsExpr "form."}}" {{end}}>
                <el-upload {{if eq $column.Immutable "1"}}:disabled="true" {{end}}
                           v-loading="upLoading"
                           class="avatar-uploader"
                           :action="apiUrl+'/system/upload/upImg'"
                           :show-file-list="false"
                           name="file"
                           :data="setUpData()"
                           :on-success="handleAvatarSuccess"
                           :before-upload="beforeAvatarUpload"
                >
                    <img v-if="{{$column.HtmlField}}Blob" :src="{{$column.HtmlField}}Blob" class="avatar" />
                    <i v-else class="el-icon-plus avatar-uploader-icon"></i>
                </el-upload>
            </el-form-item>

            {{      else if eq $column.HtmlType "richtext"}}
            <el-form-item label="{{$column.ColumnComment}}" prop="{{$column.HtmlField}}" {{if $column.IsShow}}v-show="{{$column.IsShow|JsExpr "form."}}" {{end}}>
                {{if eq $column.Immutable "1"}}
                    <div v-html="form.{{$column.HtmlField}}" style="border: lightgray solid 1px;min-height: 40px;"></div>
                {{else}}
                    <Editor v-model="form.{{$column.HtmlField}}" />
                {{end}}
            </el-form-item>
            {{      end}}
            {{  end}}
            {{end}}
        </el-form>
        {{if .table.Tabs|len|ne 0}}
        <el-tabs type="border-card">
            {{range $index, $column := .table.Tabs}}
            {{  if in $column.HtmlType "annex" "multi"}}
            <el-tab-pane label="{{$column.ColumnComment}}">
                <{{$column.LinkTableClass|CaseCamelLower}} :{{$column.LinkLabelId|CaseCamelLower}}="form.{{$column.LinkKey|CaseCamelLower}}" @before="precreate" {{if eq $column.Immutable "1"}}:disabled="true" {{end}}></{{$column.LinkTableClass|CaseCamelLower}}>
            </el-tab-pane>
            {{  end}}
            {{end}}
        </el-tabs>
        {{end}}
    </div>
</template>

<script>
{{$plugin:=""}}
{{if ContainsI $.table.PackageName "plugins"}}
{{$plugin = "plugins/"}}
{{end}}

import {
    get{{.table.ClassName}},
    create{{.table.ClassName}},
    update{{.table.ClassName}},
    {{range $index,$column:= .table.Columns}}
    {{  if and (eq $column.HtmlType "enable") (eq $column.IsList "1") }}
    change{{$.table.ClassName}}{{$column.GoField}},
    {{  end}}

    {{  if and (ne $column.LinkTableName "")}}
    list{{$column.HtmlField|CaseCamel}},
    {{  end}}
    {{end}}
} from "@/api/{{$plugin}}{{.table.ModuleName}}/{{.table.BusinessName | CaseCamelLower}}";

{{if $hasImageFile}}
import { getToken } from "@/utils/auth";
{{end}}

{{if $hasTreeSelect}}
import Treeselect from "@riophae/vue-treeselect";
import "@riophae/vue-treeselect/dist/vue-treeselect.css";
{{end}}

import { getUserProfile } from "@/api/system/user";
import {genCode} from '@/api/code.js';
import dayjs from 'dayjs'

export default {
    name: "{{.table.ClassName}}Editor",
    components:{
        {{if $hasRichText}}Editor:()=>import('@/components/Editor'),{{end}}
        {{if $hasTreeSelect}}Treeselect,{{end}}

        {{range $index, $column := .table.Columns}}
        {{  if eq $column.HtmlType "refer"}}
        {{$column.HtmlField}}Refer:()=>import("@/views/{{$column.LinkTableModule}}/{{$column.LinkTableClass|CaseCamelLower}}/list/refer.vue"),
        {{  end}}
        {{end}}

        {{range $index, $column := .table.Tabs}}
        {{  if in $column.HtmlType "annex" "multi"}}
        {{$column.LinkTableClass|CaseCamelLower}}:()=>import("@/views/{{$column.LinkTableModule}}/{{$column.LinkTableClass|CaseCamelLower}}/list/index.vue"),
        {{  end}}
        {{end}}
    },
    props: [
        {{range $index, $column := .table.Columns}}
        {{  if eq $column.HtmlType "underlying"}}
        "{{$column.HtmlField}}",
        {{  end}}
        {{end}}
    ],
    data() {
        return {
            {{range $index, $column := .table.Columns}}
            {{  if ne $column.DictType ""}}
            // {{$column.HtmlField}}Options字典数据
            {{$column.HtmlField}}Options: [],

            {{  else if ne $column.LinkTableName ""}}
            // {{$column.HtmlField}}Options关联表数据
            {{$column.HtmlField}}Options: [],
            {{  end}}

            {{  if eq $column.HtmlType "duration"}}
            {{$column.HtmlField}}: {
                hour: 0,
                minute: 0,
                second: 0
            },
            {{  end}}

            {{  if eq $column.HtmlType "treeselect"}}
            // {{$column.HtmlField}}Tree关联表数据
            {{$column.HtmlField}}Tree: [],
            {{  end}}

            {{  if eq $column.HtmlType "imagefile"}}
            // 图片上传地址
            {{$column.HtmlField}}Blob: "",
            // 上传加载
            upLoading: false,
            {{  end}}

            {{  if eq $column.HtmlType "refer"}}
            {{$column.HtmlField}}Open:false,
            {{$column.LabelField|CaseCamelLower}}: "",
            {{  end}}
            {{end}}
            // 表单参数
            form: {},
            params: {},
            // 表单校验
            rules: {
                {{range $index, $column := .table.Columns}}
                    {{if and (eq $column.IsRequired "1") (ne $column.HtmlType "underlying")}}
                {{$column.HtmlField}} : [{ required: true, message: "{{$column.ColumnComment}}不能为空", trigger: "blur" }]{{if $.table.Columns|len|ne $index}},{{end}}
                    {{end}}
                {{end}}
            },
            user:{}
        };
    },
    methods: {
        // 表单重置
        async reset() {
            {{if $linkUser}}
            await this.getUser();
            {{end}}
            // 重新加载选项
            {{range $index, $column := .table.Columns}}
            {{  if ne $column.DictType ""}}
            await this.getDicts("{{$column.DictType}}").then(response => {
                this.{{$column.HtmlField}}Options = response.data.values||[];
            });
            {{  else if and (ne $column.LinkTableName "") (ni $column.HtmlType "refer" "extend" "annex" "multi") }}
            await this.get{{$column.HtmlField|CaseCamel}}Items()
            {{  end}}
            {{end}}
            {{if $hasTreeSelect}}
            this.getTreeselect();
            {{end}}
            // 重置表单值
            this.form = {
                {{range $index, $column := .table.Columns}}
                {{  if $column.Default}}
                {{$column.HtmlField}}: "{{$column.Default}}" ,
                {{  else if $column.Deliver}}
                {{$column.HtmlField}}: this.params.{{$column.Deliver|CaseCamelLower}}.toString(),
                {{  else if eq $column.Special "current_user"}}
                {{$column.HtmlField}}: ''+this.user.id,
                {{  else if eq $column.Special "current_dept"}}
                {{$column.HtmlField}}: ''+this.user.deptId,
                {{  else if eq $column.Special "now"}}
                {{$column.HtmlField}}: dayjs(new Date).format("YYYY-MM-DD HH:mm:ss"),
                {{  else if eq $column.HtmlType "radio"}}
                {{$column.HtmlField}}: "0" ,
                {{  else if eq $column.HtmlType "checkbox"}}
                {{$column.HtmlField}}: [] ,
                {{  else}}
                {{$column.HtmlField}}: undefined,
                {{  end}}
                {{end}}
            };
            // 重置相关参数
            {{range $index, $column := .table.Columns}}
            {{  if eq $column.Special "current_user"}}
            this.{{$column.LabelField|CaseCamelLower}} = this.user.userNickname
            {{  else if eq $column.Special "current_dept"}}
            this.{{$column.LabelField|CaseCamelLower}} = this.user.dept.deptName
            {{  else if eq $column.HtmlType "imagefile"}}
            this.{{$column.HtmlField}}Blob = ''
            {{  else if $column.DeliverLabel}}
            this.{{$column.LabelField|CaseCamelLower}} = this.params.{{$column.DeliverLabel|CaseCamelLower}}
            {{  end}}
            {{end}}
            this.resetForm("form");
        },
        {{if .action}}
        {{  if eq .action.Type "push"}}
        show(){
            this.reset();
        },
        {{  else if eq .action.Type "refer"}}
        show(params){
            this.params = params;
            this.reset();
        },
        {{  else}}
        async show(params){
            await this.reset();
            get{{.table.ClassName}}(params.{{.table.PkColumn.HtmlField}}).then(response => {
                let data = response.data;
                {{range $index, $column := .table.Columns}}
                {{  if .Default}}
                data.{{.HtmlField}} = '{{.Default}}';
                {{  else if eq .HtmlType "checkbox"}}
                {{      if eq .GoType "int"}}
                function bitcode(v){
                    let res = []
                    if(v&1){
                        res.push("1")
                    }
                    if(v&2){
                        res.push("2")
                    }
                    return res
                }
                data.{{.HtmlField}} = bitcode(data.{{.HtmlField}})
                {{      else}}
                data.{{.HtmlField}} = data.{{.HtmlField}}.split(",")
                {{      end}}
                {{  else if eq .HtmlType "radio" "select"}}
                data.{{.HtmlField}} = ''+data.{{.HtmlField}}
                {{  else if eq .HtmlType "imagefile"}}
                    this.{{.HtmlField}}Blob = this.getUpFileUrl(this.apiUrl, data.{{.HtmlField}});
                {{  end}}
                {{end}}
                this.form = data;
                {{range $index, $column := .table.Columns}}
                {{  if eq $column.HtmlType "refer"}}
                this.{{$column.LabelField|CaseCamelLower}} = data.{{$column.LabelField|CaseCamelLower}}
                {{  end}}
                {{end}}
            });
        },
        {{  end}}
        {{end}}
        // 提前提交
        precreate(){
            this.$refs["form"].validate(valid => {
                if (valid) {
                    if (!this.form.{{$.table.PkColumn.HtmlField}}) {
                        create{{.table.ClassName}}(this.form).then(response => {
                            if (response.code === 0) {
                                this.form.{{$.table.PkColumn.HtmlField}} = response.data.{{$.table.PkColumn.HtmlField}};
                            } else {
                                this.msgError(response.msg);
                            }
                        });
                    }
                }
            });
        },
        // 正常提交
        submitForm: function(done) {
            this.$refs["form"].validate(valid => {
                if (valid) {
                    {{range $index, $column := .table.Columns}}
                    {{if eq $column.HtmlType "underlying"}}
                    this.form.{{$column.HtmlField}} = this.{{$column.HtmlField}};
                    {{end}}
                    {{end}}
                    if (this.form.{{.table.PkColumn.HtmlField}} != undefined) {
                        update{{.table.ClassName}}(this.form).then(response => {
                            if (response.code === 0) {
                                this.msgSuccess("修改成功");
                                done()
                            } else {
                                this.msgError(response.msg);
                            }
                        });
                    } else {
                        create{{.table.ClassName}}(this.form).then(response => {
                            if (response.code === 0) {
                                this.msgSuccess("新增成功");
                                done()
                            } else {
                                this.msgError(response.msg);
                            }
                        });
                    }
                }
            });
        },
        genCode(){
            {{range $index, $column := .table.Columns}}
            {{  if eq $column.HtmlType "code"}}
            genCode("{{$column.CodeRule}}").then(response=>{
                if (response.code === 0) {
                    this.form.{{$column.HtmlField}} = response.data;
                } else {
                    this.msgError(response.msg);
                }
            })
            {{  end}}
            {{end}}
        },
        getUser() {
            return getUserProfile().then(response => {
                this.user = response.data;
            });
        },

        {{/* 不同类型字段的公共方法 */}}
        {{if $hasDuration}}
        // 将时长(对象)转换成时长(秒)
        convertDuration(v){
            return parseInt(v.hour) * 3600 + parseInt(v.minute) * 60 + parseInt(v.second)
        },
        // 将时长(秒)转换成时长(对象)
        convertDurationObject(v) {
            let hour = Math.floor(v/3600)
            let minute = Math.floor(v/60) - hour * 60
            let second = Math.floor(v) - hour * 3600 - minute * 60
            return {
                hour:hour,
                minute:minute,
                second:second,
            }
        },
        // 将时长(秒)转换成时长(字符串)
        convertDurationString(v){
            let hour = Math.floor(v/3600)
            let minute = Math.floor(v/60) - hour * 60
            let second = Math.floor(v) - hour * 3600 - minute * 60
            let str = ""
            if(hour){
                str+=hour+"小时"
            }
            if(minute){
                str+=minute+"分钟"
            }
            if(second){
                str+=second+"秒"
            }
            return str
        },
        {{end}}

        {{if $hasTreeSelect}}
        /** 转换树状数据结构 */
        normalizer(node) {
            if (node.children && !node.children.length) {
                delete node.children;
            }
            return {
                id: node.id,
                label: node.name,
                children: node.children
            };
        },
        /** 查询树状结构 */
        getTreeselect() {
            {{range $index, $column := .table.Columns}}
            {{  if eq $column.HtmlType "treeselect"}}
            list{{$column.HtmlField|CaseCamel}}(this.queryParams).then(response => {
                this.{{$column.HtmlField}}Tree = [];
                const data = { id: 0, name: '顶级节点', children: [] };
                data.children = this.handleTree(response.data.list||[], "id", "parentId");
                this.{{$column.HtmlField}}Tree.push(data);
            });
            {{  end}}
            {{end}}
        },
        {{end}}

        {{/* 各个字段专用的方法 */}}
        {{range $index, $column := .table.Columns}}
        {{  if ne $column.LinkTableName ""}}
        //关联{{$column.LinkTableName}}表选项
        get{{$column.HtmlField|CaseCamel}}Items() {
            return this.getItems(list{{$column.HtmlField|CaseCamel}}, {pageSize:10000}).then(res => {
                this.{{$column.HtmlField}}Options = this.setItems(res, '{{$column.LinkLabelId}}', '{{$column.LinkLabelName}}')
            })
        },
        {{  end}}

        {{  if eq $column.HtmlType "duration"}}
        {{$column.HtmlField}}Change(){
            this.form.{{$column.HtmlField}} = this.convertDuration(this.{{$column.HtmlField}})
        },
        {{  end}}

        {{  if eq $column.HtmlType "imagefile"}}
        handleAvatarSuccess(res, file) {
            if (res.code === 0) {
                this.{{$column.HtmlField}}Blob = URL.createObjectURL(file.raw);
                this.form.{{$column.HtmlField}} = res.data.fileInfo.fileUrl;
            } else {
                this.msgError(res.msg);
            }
            this.upLoading = false;
        },
        beforeAvatarUpload(file) {
            this.upLoading = true;
            return true;
        },
        setUpData() {
            return { token: getToken() };
        },
        {{  end}}

        {{  if eq $column.HtmlType "refer"}}
        {{$column.HtmlField}}Confirm(){
            let row = this.$refs.{{$column.HtmlField}}Refer.referredRow;
            this.form.{{$column.HtmlField}} = row.{{$column.LinkLabelId}};
            this.{{$column.LabelField|CaseCamelLower}} = row.{{$column.LinkLabelName}};
            this.{{$column.HtmlField}}Open = false;
        },
        {{$column.HtmlField}}Cancel() {
            this.{{$column.HtmlField}}Open = false;
        },
        {{  end}}
        {{end}}
    }
};
</script>

<style>
    {{if $hasDuration}}
    .duration .el-input {
        width: 130px;
    }
    {{end}}

    {{if $hasImageFile}}
    .avatar-uploader .el-upload {
        border: 1px dashed #d9d9d9;
        border-radius: 6px;
        cursor: pointer;
        position: relative;
        overflow: hidden;
    }
    .avatar-uploader .el-upload:hover {
        border-color: #409eff;
    }
    .avatar-uploader-icon {
        font-size: 28px;
        color: #8c939d;
        width: 178px;
        height: 178px;
        line-height: 178px;
        text-align: center;
    }
    .avatar {
        width: 178px;
        height: 178px;
        display: block;
    }
    {{end}}

    {{if $hasRichText}}
    .quill-file {
        height: 0;
    }

    .quill-editor {
        height: auto !important;
    }
    {{end}}
</style>