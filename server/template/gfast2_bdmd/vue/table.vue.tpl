{{/*
列表页的模板,默认为普通的列表页
参数:
table: *Table
    TplCategory 为"tree"时，表示作为树状表格
    Columns中包含HtmlType为"underlying"的数据时，表示作为annex字段的列表
isRefer: bool 为true时，表示作为字段关联的单选列表
isMulti: bool 为true时，表示代替字段的annex列表
*/}}

{{$hasDuration:=false}}
{{$hasUnderlying:=false}}
{{$hasQuery:=false}}
{{range $index,$column:=.table.Columns}}
    {{if eq $column.HtmlType "duration"}}{{$hasDuration = true}}{{end}}
    {{if eq $column.HtmlType "underlying"}}{{$hasUnderlying = true}}{{end}}
    {{if and (eq $column.IsQuery "1") (ne $column.ColumnName "created_by") (ne $column.ColumnName "updated_by") (ne $column.ColumnName "created_at") (ne $column.ColumnName "updated_at") (ne $column.ColumnName "deleted_at") (ne $column.HtmlType "underlying")}}
        {{$hasQuery = true}}
    {{end}}
{{end}}
{{$tabs := .table.Tabs|len}}

<template>
    {{$lens := .table.Columns|len}}
    {{$businessName := .table.BusinessName | CaseCamelLower}}
    <div class="app-container">
        {{if .table.Description}}
        <el-alert title="{{.table.Description}}" type="success" close-text="了解" style="margin-bottom: 8px;"></el-alert>
        {{end}}
        {{if $hasQuery}}
        <el-form :model="queryParams" ref="queryForm" :inline="true" label-width="68px">
            {{range $index, $column := .table.Columns}}
            {{  if and (eq $column.IsQuery "1") (ne $column.ColumnName "created_by") (ne $column.ColumnName "updated_by") (ne $column.ColumnName "created_at") (ne $column.ColumnName "updated_at") (ne $column.ColumnName "deleted_at") (ne $column.HtmlType "underlying")}}
            {{      if eq $column.HtmlType "input" "textarea"}}
            <el-form-item label="{{$column.ColumnComment}}" prop="{{$column.HtmlField}}">
                <el-input
                    v-model="queryParams.{{$column.HtmlField}}"
                    placeholder="请输入{{$column.ColumnComment}}"
                    clearable
                    size="small"
                    @keyup.enter.native="handleQuery"
                />
            </el-form-item>

            {{      else if and (eq $column.HtmlType "select" "radio" "checkbox") (ne $column.DictType "") }}
            <el-form-item label="{{$column.ColumnComment}}" prop="{{$column.HtmlField}}">
                <el-select v-model="queryParams.{{$column.HtmlField}}" placeholder="请选择{{$column.ColumnComment}}" clearable size="small">
                    <el-option
                        v-for="dict in {{$column.HtmlField}}Options"
                        :key="dict.key"
                        :label="dict.value"
                        :value="dict.key"
                    />
                </el-select>
            </el-form-item>

            {{      else if eq $column.HtmlType "datetime"}}
            <el-form-item label="{{$column.ColumnComment}}" prop="{{$column.HtmlField}}">
                <el-date-picker
                    clearable size="small" style="width: 200px"
                    v-model="queryParams.{{$column.HtmlField}}"
                    type="date"
                    value-format="yyyy-MM-dd"
                    placeholder="选择{{$column.ColumnComment}}">
                </el-date-picker>
            </el-form-item>

            {{      else if and (eq $column.HtmlType "select" "radio" "checkbox") (ne $column.LinkTableName "")}}
            <el-form-item label="{{$column.ColumnComment}}" prop="{{$column.HtmlField}}">
                <el-select v-model="queryParams.{{$column.HtmlField}}" placeholder="请选择{{$column.ColumnComment}}" clearable size="small">
                    <el-option
                        v-for="item in {{$column.HtmlField}}Options"
                        :key="item.key"
                        :label="item.value"
                        :value="item.key"
                    />
                </el-select>
            </el-form-item>

            {{      else}}
            <el-form-item label="{{$column.ColumnComment}}" prop="{{$column.HtmlField}}">
                <el-select v-model="queryParams.{{$column.HtmlField}}" placeholder="请选择{{$column.ColumnComment}}" clearable size="small">
                    <el-option label="请选择字典生成" value="" />
                </el-select>
            </el-form-item>
            {{      end}}
            {{  end}}
            {{end}}
            <el-form-item>
                <el-button type="primary" icon="el-icon-search" size="mini" @click="handleQuery">搜索</el-button>
                <el-button icon="el-icon-refresh" size="mini" @click="resetQuery">重置</el-button>
            </el-form-item>
        </el-form>
        {{end}}

        <el-row :gutter="10" class="mb8">
            {{range .table.Actions}}
            {{  if eq .Type "push"}}
            <el-col :span="1.5">
                <el-button
                    type="primary"
                    icon="{{.Icon}}"
                    size="mini"
                    @click="handle{{.Name|CaseCamel}}"
                    v-hasPermi="['{{$.table.ModuleName}}/{{$businessName}}/{{.Name}}']"
                    v-show="!disabled"
                >{{.Abbr}}</el-button>
            </el-col>
            {{  end}}
            {{end}}
            <el-col :span="1.5">
                <el-button
                    type="danger"
                    icon="el-icon-delete"
                    size="mini"
                    :disabled="multiple"
                    @click="handleDelete"
                    v-hasPermi="['{{.table.ModuleName}}/{{$businessName}}/delete']"
                    v-show="!disabled"
                >删除</el-button>
            </el-col>
        </el-row>

        <el-table v-loading="loading" :data="{{$businessName}}List"
            @selection-change="handleSelectionChange"
            {{if eq .table.TplCategory "tree"}}
            row-key="{{.table.TreeCode}}"
            default-expand-all
            :tree-props="{children: 'children', hasChildren: 'hasChildren'}"
            {{end}}
            {{if .isRefer}}@row-click="onClickRow"{{end}}
        >

            {{if .isRefer}}
            <el-table-column label="" width="65">
                <template slot-scope="scope">
                    <el-radio class="radio" v-model="referredId" :label="scope.row.id">&nbsp;</el-radio>
                </template>
            </el-table-column>
            {{else}}
            <el-table-column type="selection" width="55" align="center" />
            {{end}}

            {{range $index, $column := .table.Columns}}
            {{  if eq $column.IsPk "1"}}
            {{      if eq $column.IsList "1"}}
            <el-table-column label="{{$column.ColumnComment}}" align="center" prop="{{$column.HtmlField}}" />
            {{      end}}
            {{  else if eq $column.HtmlType "underlying"}}
            <!-- 隐含字段{{$column.ColumnComment}} -->
            {{  else if and (eq $column.IsList "1") (eq $column.HtmlType "date")}}
            <el-table-column label="{{$column.ColumnComment}}" align="center" prop="{{$column.HtmlField}}" width="180">
                <template slot-scope="scope">
                    <span>{{VueTag "{{"}} parseTime(scope.row.{{$column.HtmlField}}, '{y}-{m}-{d}') {{VueTag "}}"}}</span>
                </template>
            </el-table-column>
            {{  else if and (eq $column.IsList "1") (eq $column.HtmlType "time")}}
            <el-table-column label="{{$column.ColumnComment}}" align="center" prop="{{$column.HtmlField}}" width="180">
                <template slot-scope="scope">
                    <span>{{VueTag "{{"}} parseTime(scope.row.{{$column.HtmlField}}, '{h}:{i}:{s}') {{VueTag "}}"}}</span>
                </template>
            </el-table-column>
            {{  else if and (eq $column.IsList "1") (eq $column.HtmlType "datetime")}}
            <el-table-column label="{{$column.ColumnComment}}" align="center" prop="{{$column.HtmlField}}" width="180">
                <template slot-scope="scope">
                    <span>{{VueTag "{{"}} parseTime(scope.row.{{$column.HtmlField}}, '{y}-{m}-{d} {h}:{i}:{s}') {{VueTag "}}"}}</span>
                </template>
            </el-table-column>
            {{  else if and (eq $column.IsList "1") (eq $column.HtmlType "duration")}}
            <el-table-column label="{{$column.ColumnComment}}" align="center" prop="{{$column.HtmlField}}" width="180">
                <template slot-scope="scope">
                    <span>{{VueTag "{{"}} duration(scope.row.{{$column.HtmlField}}) {{VueTag "}}"}}</span>
                </template>
            </el-table-column>
            {{  else if and (eq $column.IsList "1") (eq $column.HtmlType "imagefile")}}
            <el-table-column label="{{$column.ColumnComment}}" align="center" prop="{{$column.HtmlField}}" width="180">
                <template slot-scope="scope">
                    <img :src="getUpFileUrl(apiUrl, scope.row.{{$column.HtmlField}})" style="width: 80px;max-height: 80px;"/>
                </template>
            </el-table-column>
            {{  else if and (eq $column.IsList "1") (eq $column.HtmlType "enable")}}
            <el-table-column label="{{$column.ColumnComment}}" align="center">
                <template slot-scope="scope">
                    <el-switch
                        v-model="scope.row.{{$column.HtmlField}}"
                        :active-value="1"
                        :inactive-value="0"
                        @change="{{$column.HtmlField}}Change(scope.row)"
                    ></el-switch>
                </template>
            </el-table-column>
            {{  else if and (eq $column.IsList "1") (eq $column.HtmlType "refer")}}
            <el-table-column label="{{$column.ColumnComment}}" align="center" prop="{{$column.LabelField|CaseCamelLower}}" />
            {{  else if and (eq $column.IsList "1") (eq $column.HtmlType "extend")}}
            <!-- extend字段{{$column.ColumnComment}} -->
            {{  else if and (eq $column.IsList "1") (ne $column.LinkTableName "")}}
            <el-table-column label="{{$column.ColumnComment}}" align="center" prop="{{$column.LabelField|CaseCamelLower}}" width="100"/>
            {{  else if and (eq $column.IsList "1") (ne $column.DictType "")}}
            <el-table-column label="{{$column.ColumnComment}}" align="center" prop="{{$column.LabelField|CaseCamelLower}}" />
            {{  else if and (eq $column.IsList "1") (ne $column.HtmlField "")}}
            <el-table-column label="{{$column.ColumnComment}}" align="center" prop="{{$column.HtmlField}}" />
            {{  end}}
            {{end}}
            {{range $index, $column := .table.Tabs}}
            {{  if eq $column.IsList "1" }}
            <el-table-column label="{{$column.ColumnComment}}" align="center">
                <template slot-scope="scope">
                    <span>{{VueTag "{{"}} (scope.row.{{$column.HtmlField}}||[]).map(v=>{return v.{{$column.LinkLabelName}}}).join(",") {{VueTag "}}"}}</span>
                </template>
            </el-table-column>
            {{  end}}
            {{end}}
            <el-table-column label="操作" align="center" class-name="small-padding fixed-width">
                <template slot-scope="scope">
                    {{range .table.Actions}}
                    {{  if in .Type "view" "edit" "patch" "refer"}}
                    <el-button
                        size="mini"
                        type="text"
                        icon="{{.Icon}}"
                        @click="handle{{.Name|CaseCamel}}(scope.row)"
                        v-hasPermi="['{{.ModuleName}}/{{$businessName}}/{{.Name}}']"
                        {{if eq .Type "view"}}
                            {{if .IsValid}}v-show="{{.IsValid|JsExpr "scope.row."}}" {{end}}
                        {{else}}
                            v-show="!disabled{{if .IsValid}}&&{{.IsValid|JsExpr "scope.row."}}{{end}}"
                        {{end}}
                    >{{.Abbr}}</el-button>
                    {{  end}}
                    {{end}}
                    <el-button
                            size="mini"
                            type="text"
                            icon="el-icon-delete"
                            @click="handleDelete(scope.row)"
                            v-hasPermi="['{{.table.ModuleName}}/{{$businessName}}/delete']"
                            v-show="!disabled"
                    >删除</el-button>
                </template>
            </el-table-column>
        </el-table>
        {{if ne .table.TplCategory "tree"}}
        <pagination
            v-show="total>0"
            :total="total"
            :page.sync="queryParams.pageNum"
            :limit.sync="queryParams.pageSize"
            @pagination="getList"
        />
        {{end}}

        {{range .table.Actions}}
        {{  if and $.isMulti (eq .Name "create")}}
        <el-dialog title="从已有{{$.table.FunctionName}}中选择或新增{{$.table.FunctionName}}"
            :visible.sync="{{.Name|CaseCamelLower}}Open" width="800px" append-to-body :close-on-click-modal="false">
            <multiple ref="multiple"></multiple>
            <div slot="footer" class="dialog-footer">
                <el-button type="primary" @click="{{.Name|CaseCamelLower}}Confirm">确 定</el-button>
                <el-button @click="{{.Name|CaseCamelLower}}Cancel">取 消</el-button>
            </div>
        </el-dialog>
        {{  else if in .Type "push" "view" "edit" "patch" "refer"}}
        <el-dialog title="{{.Title}}" :visible.sync="{{.Name|CaseCamelLower}}Open" width="800px" append-to-body :close-on-click-modal="false">
            <{{.Name|CaseCamelLower}}Editor ref="{{.Name|CaseCamelLower}}Editor"
                {{range $index, $column := .Table.Columns}}
                    {{  if eq $column.HtmlType "underlying"}}
                :{{$column.HtmlField}}="{{$column.HtmlField}}"
                    {{  end}}
                {{end}}
            />
            <div slot="footer" class="dialog-footer">
                <el-button type="primary" @click="{{.Name|CaseCamelLower}}Confirm">确 定</el-button>
                <el-button @click="{{.Name|CaseCamelLower}}Cancel">取 消</el-button>
            </div>
        </el-dialog>
        {{  end}}
        {{end}}
    </div>
</template>

<script>
{{$plugin:=""}}
{{if ContainsI $.table.PackageName "plugins"}}
{{$plugin = "plugins/"}}
{{end}}

import {
    list{{.table.ClassName}},
    del{{.table.ClassName}},
    {{if .isMulti}}select{{.table.ClassName}},{{end}}
    {{range $index,$column:= .table.Columns}}
    {{  if and (eq $column.HtmlType "enable") (eq $column.IsList "1") }}
    change{{$.table.ClassName}}{{$column.GoField}},
    {{  end}}
    {{  if and (ne $column.LinkTableName "")}}
    list{{$column.HtmlField|CaseCamel}},
    {{  end}}
    {{end}}
} from "@/api/{{$plugin}}{{.table.ModuleName}}/{{$businessName}}";

export default {
    name: "{{.table.ClassName}}",
    components:{
        {{if not .action}}
        {{  range .table.Actions}}
        {{      if and $.isMulti (eq .Name "create")}}
        {{          with $column := index $.table.Columns 2}}
        multiple:()=>import("@/views/{{$column.LinkTableModule}}/{{$column.LinkTableClass|CaseCamelLower}}/list/index.vue"),
        {{          end}}
        {{      else}}
        {{.Name|CaseCamelLower}}Editor:()=>import('../{{.Name|CaseCamelLower}}/form.vue'),
        {{      end}}
        {{  end}}
        {{end}}
    },
    props: [
        {{range $index, $column := .table.Columns}}
        {{  if eq $column.HtmlType "underlying"}}
        "{{$column.HtmlField}}",
        {{  end}}
        {{end}}
        "disabled" // 禁用列表页面的增删改按钮
    ],
    data() {
        return {
            {{range $index, $column := .table.Columns}}
            {{  if and (eq $column.IsQuery "1") (ne $column.ColumnName "created_by") (ne $column.ColumnName "updated_by") (ne $column.ColumnName "created_at") (ne $column.ColumnName "updated_at") (ne $column.ColumnName "deleted_at") (ne $column.HtmlType "underlying")}}
            {{      if ne $column.DictType ""}}
            // {{$column.HtmlField}}Options字典数据
            {{$column.HtmlField}}Options: [],

            {{      else if ne $column.LinkTableName ""}}
            // {{$column.HtmlField}}Options关联表数据
            {{$column.HtmlField}}Options: [],
            {{      end}}
            {{  end}}
            {{end}}
            // 遮罩层
            loading: true,
            // 选中数组
            ids: [],
            // 非单个禁用
            single: true,
            // 非多个禁用
            multiple: true,
            // 总条数
            total: 0,
            // {{.table.FunctionName}}表格数据
            {{$businessName}}List: [],
            // 是否显示弹出层
            {{range .table.Actions}}
            {{.Name|CaseCamelLower}}Open: false,
            {{end}}
            // 查询参数
            queryParams: {
                pageNum: 1,
                pageSize: 10,
                {{range $index, $column := .table.Columns}}
                {{  if eq $column.IsQuery "1"}}
                {{$column.HtmlField}}: undefined{{if ne $lens $index}},{{end}}
                {{  end}}
                {{end}}
            },
{{/*            {{if .action}}params: {},{{end}}*/}}
            {{if .isRefer}}
            referredId: null,
            referredRow: null,
            {{end}}
        };
    },
    created() {
        {{range $index, $column := .table.Columns}}
        {{  if and (eq $column.IsQuery "1") (ne $column.ColumnName "created_by") (ne $column.ColumnName "updated_by") (ne $column.ColumnName "created_at") (ne $column.ColumnName "updated_at") (ne $column.ColumnName "deleted_at") (ne $column.HtmlType "underlying")}}
        {{      if ne $column.DictType ""}}
        this.getDicts("{{$column.DictType}}").then(response => {
            this.{{$column.HtmlField}}Options = response.data.values||[];
        });
        {{      else if ne $column.LinkTableName ""}}
        this.get{{$column.HtmlField|CaseCamel}}Items()
        {{      end}}
        {{  end}}
        {{end}}

        this.getList();
        {{if .isRefer}}
        this.referredId = this.id;
        {{end}}
    },
    watch:{
        {{if .isRefer}}
        id() {
            if (this.referredId != this.id) {
                this.referredId = this.id;
            }
        },
        {{end}}

        {{range $index, $column := .table.Columns}}
        {{  if eq $column.HtmlType "underlying"}}
        {{$column.HtmlField}}(){
            this.getList()
        },
        {{  end}}
        {{end}}
    },
    methods: {
        {{range $index, $column := .table.Columns}}
        {{  if and (eq $column.HtmlType "enable") (eq $column.IsList "1")}}
        // {{$column.ColumnComment}}修改
        {{$column.HtmlField}}Change(row) {
            let text = row.{{$column.HtmlField}} === 1 ? "启用" : "停用";
            this.$confirm('确认要"' + text + '"吗?', "警告", {
                confirmButtonText: "确定",
                cancelButtonText: "取消",
                type: "warning"
            }).then(function() {
                return change{{$.table.ClassName}}{{$column.GoField}}(row.{{$.table.PkColumn.HtmlField}}, row.{{$column.HtmlField}});
            }).then(() => {
                this.msgSuccess(text + "成功");
            }).catch(function() {
                row.userStatus =row.userStatus === 0 ?1 : 0;
            });
        },
        {{  end}}

        {{  if ne $column.LinkTableName ""}}
        //关联{{$column.LinkTableName}}表选项
        get{{$column.HtmlField|CaseCamel}}Items() {
            this.getItems(list{{$column.HtmlField|CaseCamel}}, {pageSize:10000}).then(res => {
                this.{{$column.HtmlField}}Options = this.setItems(res, '{{$column.LinkLabelId}}', '{{$column.LinkLabelName}}')
            })
        },
        {{  end}}
        {{end}}
        /** 查询{{.table.FunctionName}}列表 */
        getList() {
            this.loading = true;
            {{range $index, $column := .table.Columns}}
            {{  if eq $column.HtmlType "underlying"}}
            if(!this.{{$column.HtmlField}}){
                this.{{$businessName}}List = [];
                this.total = 0
                this.loading = false
                return
            }
            this.queryParams.{{$column.HtmlField}} = this.{{$column.HtmlField}};
            {{  end}}
            {{end}}
            list{{.table.ClassName}}(this.queryParams).then(response => {
                {{if eq .table.TplCategory "tree"}}
                this.{{$businessName}}List = this.handleTree(response.data.list||[], "{{$.table.TreeCode}}", "{{$.table.TreeParentCode}}");
                {{else}}
                this.{{$businessName}}List = response.data.list || [];
                this.total = response.data.total;
                {{end}}
                this.loading = false;
            });
        },
        {{if $hasDuration}}
        // 将时长(秒)转换成时长(字符串)
        duration(v){
            let hour = Math.floor(v/3600)
            let minute = Math.floor(v/60) - hour * 60
            let second = Math.floor(v) - hour * 3600 - minute * 60
            let str = ""
            if(hour){
                str+=hour+"小时"
            }
            if(minute){
                str+=minute+"分钟"
            }
            if(second){
                str+=second+"秒"
            }
            return str
        },
        {{end}}
        /** 搜索按钮操作 */
        handleQuery() {
            this.queryParams.pageNum = 1;
            this.getList();
        },
        /** 重置按钮操作 */
        resetQuery() {
            this.resetForm("queryForm");
            this.handleQuery();
        },
        // 多选框选中数据
        handleSelectionChange(selection) {
            this.ids = selection.map(item => item.{{.table.PkColumn.HtmlField}})
            this.single = selection.length!=1
            this.multiple = !selection.length
        },
        {{if .isRefer}}
        onClickRow(row) {
            this.referredId = row.id;
            this.referredRow = row;
            this.$emit("refer", row)
        },
        {{end}}
        {{if not .action}}
        {{  range .table.Actions}}
        {{      if and $.isMulti (eq .Name "create")}}
        handle{{.Name|CaseCamel}}() {
            {{range $index, $column := $.table.Columns}}
            {{  if eq $column.HtmlType "underlying"}}
            if(!this.{{$column.HtmlField}}){
                this.$emit("before")
            }
            {{  end}}
            {{end}}
            this.{{.Name|CaseCamelLower}}Open = true;
        },
        {{      else if eq .Type "push"}}
        handle{{.Name|CaseCamel}}() {
            {{range $index, $column := $.table.Columns}}
            {{  if eq $column.HtmlType "underlying"}}
            if(!this.{{$column.HtmlField}}){
                this.$emit("before")
            }
            {{  end}}
            {{end}}
            this.{{.Name|CaseCamelLower}}Open = true;
            this.$nextTick(()=>{
                this.$refs.{{.Name|CaseCamelLower}}Editor.show({})
            })
        },
        {{      else}}
        handle{{.Name|CaseCamel}}(row) {
            this.{{.Name|CaseCamelLower}}Open = true;
            this.$nextTick(()=>{
                this.$refs.{{.Name|CaseCamelLower}}Editor.show(row)
            })
        },
        {{      end}}
        {{.Name|CaseCamelLower}}Confirm: function() {
            {{if and $.isMulti (eq .Name "create")}}
            let ids = this.$refs.multiple.ids;
            select{{$.table.ClassName}}(this.{{with $column := index $.table.Columns 1}}{{$column.HtmlField}}{{end}}, ids).then(()=>{
                this.msgSuccess("操作成功");
                this.{{.Name|CaseCamelLower}}Open = false;
                this.getList()
            })
            {{else}}
            this.$refs.{{.Name|CaseCamelLower}}Editor.submitForm(()=>{
                this.{{.Name|CaseCamelLower}}Open = false;
                this.getList();
            })
            {{end}}
        },
        // 取消按钮
        {{.Name|CaseCamelLower}}Cancel() {
            this.{{.Name|CaseCamelLower}}Open = false;
        },
        {{  end}}
        {{end}}
        /** 删除按钮操作 */
        handleDelete(row) {
            const {{.table.PkColumn.HtmlField}}s = row.{{.table.PkColumn.HtmlField}} || this.ids;
            this.$confirm('是否确认删除{{.table.FunctionName}}编号为"' + {{.table.PkColumn.HtmlField}}s + '"的数据项?', "警告", {
                confirmButtonText: "确定",
                cancelButtonText: "取消",
                type: "warning"
            }).then(function() {
                return del{{.table.ClassName}}({{.table.PkColumn.HtmlField}}s);
            }).then(() => {
                this.getList();
                this.msgSuccess("删除成功");
            }).catch(function() {});
        },
    }
};
</script>

