create table if not exists `{{.model.Source.Table}}` (
    {{- range $index,$attr:= .model.Attributes}}
    {{-     if ni .Type "annex" "multi"}}{{if ne $index 0}},{{end}}
    `{{.KeyField}}` {{ if eq .Name $.model.KeyField}}int(11) auto_increment primary key
        {{- else if eq .Type "integer"}}int(11) default 0 not null
        {{- else if eq .Type "enable"}}int(11) default 0 not null
        {{- else if eq .Type "duration"}}int(11) default 0 not null
        {{- else if eq .Type "string"}}varchar({{.MaxLength}}) default '' not null
        {{- else if eq .Type "code"}}varchar({{.MaxLength}}) default '' not null
        {{- else if eq .Type "text"}}text
        {{- else if eq .Type "richtext"}}text
        {{- else if eq .Type "image"}}text
        {{- else if eq .Type "datetime"}}datetime
        {{- else if eq .Type "date"}}datetime
        {{- else if eq .Type "time"}}datetime
        {{- else if eq .Type "select"}}int(11) default 0 not null
        {{- else if eq .Type "radio"}}int(11) default 0 not null
        {{- else if eq .Type "checkbox"}}varchar(40) default '' not null
        {{- else if eq .Type "bitcode"}}int(11) default 0 not null
        {{- else if eq .Type "refer"}}int(11) default 0 not null
        {{- else if eq .Type "tree"}}int(11) default 0 not null
        {{- else}}varchar(40) default '' not null
        {{- end}} comment '{{.Title}}'
    {{-     end}}
    {{- end}}
    {{- if $.multiOrg}},
    companyCode varchar(255) not null default '' comment '组织编码'
    {{- end}}
) comment '{{.model.Title}}'
