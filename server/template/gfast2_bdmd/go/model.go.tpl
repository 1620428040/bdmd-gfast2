// ==========================================================================
// 自动生成代码，无需手动修改，重新生成会自动覆盖.
// 数据:parse.Model
// ==========================================================================
package model

import "github.com/gogf/gf/util/gmeta"

{{- if .model.HasAttrType "datetime" "date" "time"}}
import "github.com/gogf/gf/os/gtime"
{{- end}}

{{- if (.model.HasAttrType "bitcode")}}
import "strings"
{{- end}}

{{- range .model.ListAssocGroups}}
import {{.}} "gfast/app/{{.}}/model"
{{- end}}

{{/*{{- template "model" .}}*/}}
{{- template "model" set . "complex" true}}
{{- range .model.Attributes}}
{{-     if and (eq .Type "refer") .Assoc.Fields }}
{{- template "model" $.model.ExcerptReferModel .Name}}
{{-     end}}
{{- end}}

{{- define "model"}}
{{- $prefix:=""}}
{{/*{{- if .complex}}*/}}
{{/*    {{- $prefix = "Complex"}}*/}}
{{/*{{- end}}*/}}

type {{$prefix}}{{.model.Name|CaseCamel}} struct {
    gmeta.Meta `orm:"table:{{.model.Source.Table}}"`
    {{- range .model.Attributes}}
    // {{.Name}} {{.Type}} {{.Title}}
    {{-     if ni .Type "select" "refer" "extend" "hasOne" "annex" "multi" }}
    {{.KeyField|CaseCamel}} {{template "fieldType" .Type}} `orm:"{{.KeyField}}{{if eq .KeyField $.model.KeyField}},primary{{end}}" json:"{{.KeyField|CaseCamelLower}}"`
    {{-     end}}

    {{-     if eq .Type "bitcode"}}
    {{.KeyField|CaseCamel}}Label  string `json:"{{.KeyField|CaseCamelLower}}Label"`

    {{-     else if and (eq .Type "select") (not .Assoc)}}
    {{.KeyField|CaseCamel}} {{template "fieldType" .Type}} `orm:"{{.KeyField}}" json:"{{.KeyField|CaseCamelLower}}"`
    {{.KeyField|CaseCamel}}Label  string `json:"{{.KeyField|CaseCamelLower}}Label"`

    {{-     else if and (eq .Type "select") .Assoc}}
    {{.KeyField|CaseCamel}} int `orm:"{{.KeyField}}" json:"{{.KeyField|CaseCamelLower}}"`
    {{.LabelField|CaseCamel}} string `json:"{{.LabelField|CaseCamelLower}}"`
    {{.Name|CaseCamel}} *struct {
        gmeta.Meta `orm:"table:{{.Assoc.Model.Source.Table}}"`
        {{.Assoc.KeyField|CaseCamel}}   int    `orm:"{{.Assoc.KeyField}},primary" json:"{{.Assoc.KeyField|CaseCamelLower}}"` // key
        {{.Assoc.LabelField|CaseCamel}} string `orm:"{{.Assoc.LabelField}}" json:"{{.Assoc.LabelField|CaseCamelLower}}"`     // label
    } `orm:"with:{{.Assoc.KeyField}}={{.KeyField}}" json:"-"`

    {{-     else if in .Type "refer"}}
    {{.KeyField|CaseCamel}} int `orm:"{{.KeyField}}" json:"{{.KeyField|CaseCamelLower}}"`
    {{.LabelField|CaseCamel}} string `json:"{{.LabelField|CaseCamelLower}}"`
	{{-         if .Assoc.Fields}}
    {{.Name|CaseCamel}} *{{.Name|CaseCamel}}Of{{$.model.Name|CaseCamel}}  `orm:"with:{{.Assoc.KeyField}}={{.KeyField}}" json:"{{.Name|CaseCamelLower}}"`
    {{-         else}}
    {{.Name|CaseCamel}} *{{if ne .Assoc.Model.Group $.model.Group}}{{.Assoc.Model.Group}}.{{end}}{{.Assoc.Model.Name|CaseCamel}} `orm:"with:{{.Assoc.KeyField}}={{.KeyField}}" json:"{{.Name|CaseCamelLower}}"`
    {{-         end}}

    {{-     else if in .Type "extend"}}
    {{.KeyField|CaseCamel}} int `orm:"{{.KeyField}}" json:"{{.KeyField|CaseCamelLower}}"`
    *{{if ne .Assoc.Model.Group $.model.Group}}{{.Assoc.Model.Group}}.{{end}}{{.Assoc.Model.Name|CaseCamel}}  `orm:"with:{{.Assoc.KeyField}}={{.KeyField}}"`

    {{-     else if and (eq .Type "hasOne") $.complex}}
    {{.Name|CaseCamel}} *{{if ne .Assoc.Model.Group $.model.Group}}{{.Assoc.Model.Group}}.{{end}}{{.Assoc.Model.Name|CaseCamel}} `orm:"with:{{.Assoc.KeyField}}={{$.model.KeyField}}" json:"{{.Name|CaseCamelLower}}"`

    {{-     else if and (in .Type "annex" "multi") $.complex}}
    {{.Name|CaseCamel}} []*{{if ne .Assoc.Model.Group $.model.Group}}{{.Assoc.Model.Group}}.{{end}}{{.Assoc.Model.Name|CaseCamel}} `orm:"with:{{.Assoc.KeyField}}={{$.model.KeyField}}" json:"{{.Name|CaseCamelLower}}"`
    {{-     end}}
    {{- end}}
}

func (v *{{$prefix}}{{.model.Name|CaseCamel}}) Compose() {
    {{- range $index,$attr :=  .model.Attributes -}}
    {{-     if eq .Type "bitcode"}}
    {{.Name|CaseCamelLower}}List := make([]string, 0)
    {{-         range .Options}}
    if v.{{$attr.Name|CaseCamel}} & {{.Value}} != 0 {
        {{$attr.Name|CaseCamelLower}}List = append({{$attr.Name|CaseCamelLower}}List, "{{.Label}}")
    }
    {{-         end}}
    v.{{.Name|CaseCamel}}Label = strings.Join({{.Name|CaseCamelLower}}List, ",")

    {{-     else if and (eq .Type "select") (not .Assoc)}}
        {{- range .Options}}
    if v.{{$attr.Name|CaseCamel}} == {{.Value|var}} {
        v.{{$attr.Name|CaseCamel}}Label = "{{.Label}}"
    }
        {{- end}}

    {{-     else if and (eq .Type "select") .Assoc}}
    if v.{{.Name|CaseCamel}} != nil {
        v.{{.LabelField|CaseCamel}} = v.{{.Name|CaseCamel}}.{{.Assoc.LabelField|CaseCamel}}
    }

    {{-     else if eq .Type "refer"}}
    if v.{{.Name|CaseCamel}} != nil {
        v.{{.Name|CaseCamel}}.Compose()
        v.{{.LabelField|CaseCamel}} = v.{{.Name|CaseCamel}}.{{.Assoc.LabelField|CaseCamel}}
    }

    {{-     else if in .Type "annex" "multi"}}
    for _, elem := range v.{{.Name|CaseCamel}} {
        elem.Compose()
    }

    {{-     else if eq .Type "extend"}}
    if v.{{.Assoc.Model.Name|CaseCamel}} != nil {
        v.{{.Assoc.Model.Name|CaseCamel}}.Compose()
    }
    {{-     end}}
    {{- end}}
}
{{- end}}

{{- define "fieldType"}}
{{- if eq . "string"}}string
{{- else if eq . "code"}}string
{{- else if eq . "password"}}string
{{- else if eq . "text"}}string
{{- else if eq . "richtext"}}string
{{- else if eq . "integer"}}int
{{- else if eq . "enable"}}int
{{- else if eq . "bitcode"}}int
{{- else if eq . "duration"}}int
{{- else if eq . "datetime"}}*gtime.Time
{{- else if eq . "date"}}*gtime.Time
{{- else if eq . "time"}}*gtime.Time
{{- else if eq . "image"}}string
{{- else if eq . "select"}}int
{{- else if eq . "tree"}}int
{{- else if eq . "radio"}}int
{{- else if eq . "checkbox"}}string
{{- else if eq . "refer"}}int
{{- else}}string
{{- end}}
{{- end}}
