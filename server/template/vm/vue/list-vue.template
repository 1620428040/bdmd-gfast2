{{/*
列表页的模板，也作为annex类型的字段的模板
参数:
table: *Table
*/}}
<!--
{{.table.BusinessName}}
{{range .table.Columns}}
  {{.ColumnName}}  {{.HtmlType}}  {{.LinkTableName}}
{{end}}
-->

{{$hasRichText:=false}}
{{$hasTreeSelect:=false}}
{{$hasImageFile:=false}}
{{$hasDuration:=false}}
{{range $index,$column:=.table.Columns}}
    {{if eq $column.HtmlType "richtext"}}
        {{$hasRichText = true}}
    {{end}}
    {{if eq $column.HtmlType "treeselect"}}
        {{$hasTreeSelect = true}}
    {{end}}
    {{if eq $column.HtmlType "imagefile"}}
        {{$hasImageFile = true}}
    {{end}}
    {{if eq $column.HtmlType "duration"}}
        {{$hasDuration = true}}
    {{end}}
{{end}}
{{$tabs := .table.Tabs|len}}

<template>
{{$lens := .table.Columns|len}}
{{$businessName := .table.BusinessName | CaseCamelLower}}
  <div class="app-container">
    <el-form :model="queryParams" ref="queryForm" :inline="true" label-width="68px">
    {{range $index, $column := .table.Columns}}
    {{if and (eq $column.IsQuery "1") (ne $column.ColumnName "created_by") (ne $column.ColumnName "updated_by") (ne $column.ColumnName "created_at") (ne $column.ColumnName "updated_at") (ne $column.ColumnName "deleted_at") (ne $column.HtmlType "underlying")}}
    {{if eq $column.HtmlType "input" "textarea"}}
      <el-form-item label="{{$column.ColumnComment}}" prop="{{$column.HtmlField}}">
        <el-input
            v-model="queryParams.{{$column.HtmlField}}"
            placeholder="请输入{{$column.ColumnComment}}"
            clearable
            size="small"
            @keyup.enter.native="handleQuery"
        />
      </el-form-item>
      {{else if and (eq $column.HtmlType "select" "radio" "checkbox") (ne $column.DictType "") }}
      <el-form-item label="{{$column.ColumnComment}}" prop="{{$column.HtmlField}}">
        <el-select v-model="queryParams.{{$column.HtmlField}}" placeholder="请选择{{$column.ColumnComment}}" clearable size="small">
            <el-option
                v-for="dict in {{$column.HtmlField}}Options"
                :key="dict.key"
                :label="dict.value"
                :value="dict.key"
            />
        </el-select>
      </el-form-item>
      {{else if eq $column.HtmlType "datetime"}}
      <el-form-item label="{{$column.ColumnComment}}" prop="{{$column.HtmlField}}">
        <el-date-picker
            clearable size="small" style="width: 200px"
            v-model="queryParams.{{$column.HtmlField}}"
            type="date"
            value-format="yyyy-MM-dd"
            placeholder="选择{{$column.ColumnComment}}">
        </el-date-picker>
      </el-form-item>
      {{else if and (eq $column.HtmlType "select" "radio" "checkbox") (ne $column.LinkTableName "")}}
        <el-form-item label="{{$column.ColumnComment}}" prop="{{$column.HtmlField}}">
          <el-select v-model="queryParams.{{$column.HtmlField}}" placeholder="请选择{{$column.ColumnComment}}" clearable size="small">
              <el-option
                  v-for="item in {{$column.HtmlField}}Options"
                  :key="item.key"
                  :label="item.value"
                  :value="item.key"
              />
          </el-select>
        </el-form-item>
        {{else}}
        <el-form-item label="{{$column.ColumnComment}}" prop="{{$column.HtmlField}}">
          <el-select v-model="queryParams.{{$column.HtmlField}}" placeholder="请选择{{$column.ColumnComment}}" clearable size="small">
              <el-option label="请选择字典生成" value="" />
          </el-select>
        </el-form-item>
      {{end}}
      {{end}}
      {{end}}
      <el-form-item>
        <el-button type="primary" icon="el-icon-search" size="mini" @click="handleQuery">搜索</el-button>
        <el-button icon="el-icon-refresh" size="mini" @click="resetQuery">重置</el-button>
      </el-form-item>
    </el-form>

    <el-row :gutter="10" class="mb8">
      {{range .table.Actions}}
        {{if eq .RowSetting ""}}
      <el-col :span="1.5">
          <el-button
            type="primary"
            icon="{{.Icon}}"
            size="mini"
            @click="handle{{.Name|CaseCamel}}"
            v-hasPermi="['{{$.table.ModuleName}}/{{$businessName}}/{{.Name}}']"
          >{{.Abbr}}</el-button>
      </el-col>
        {{end}}
      {{end}}
      <el-col :span="1.5">
        <el-button
          type="primary"
          icon="el-icon-plus"
          size="mini"
          @click="handleAdd"
          v-hasPermi="['{{.table.ModuleName}}/{{$businessName}}/add']"
        >新增</el-button>
      </el-col>
      <el-col :span="1.5">
        <el-button
          type="success"
          icon="el-icon-edit"
          size="mini"
          :disabled="single"
          @click="handleUpdate"
          v-hasPermi="['{{.table.ModuleName}}/{{$businessName}}/edit']"
        >修改</el-button>
      </el-col>
      <el-col :span="1.5">
        <el-button
          type="danger"
          icon="el-icon-delete"
          size="mini"
          :disabled="multiple"
          @click="handleDelete"
          v-hasPermi="['{{.table.ModuleName}}/{{$businessName}}/delete']"
        >删除</el-button>
      </el-col>
    </el-row>

    <el-table v-loading="loading" :data="{{$businessName}}List" @selection-change="handleSelectionChange">
      <el-table-column type="selection" width="55" align="center" />
      {{range $index, $column := .table.Columns}}
      {{if eq $column.IsPk "1"}}
      {{if eq $column.IsList "1"}}
      <el-table-column label="{{$column.ColumnComment}}" align="center" prop="{{$column.HtmlField}}" />
      {{end}}
      {{else if eq $column.HtmlType "underlying"}}
      <!-- 隐含字段{{$column.ColumnComment}} -->
      {{else if and (eq $column.IsList "1") (eq $column.HtmlType "date")}}
      <el-table-column label="{{$column.ColumnComment}}" align="center" prop="{{$column.HtmlField}}" width="180">
        <template slot-scope="scope">
            <span>{{VueTag "{{"}} parseTime(scope.row.{{$column.HtmlField}}, '{y}-{m}-{d}') {{VueTag "}}"}}</span>
        </template>
      </el-table-column>
      {{else if and (eq $column.IsList "1") (eq $column.HtmlType "time")}}
      <el-table-column label="{{$column.ColumnComment}}" align="center" prop="{{$column.HtmlField}}" width="180">
        <template slot-scope="scope">
            <span>{{VueTag "{{"}} parseTime(scope.row.{{$column.HtmlField}}, '{h}:{i}:{s}') {{VueTag "}}"}}</span>
        </template>
      </el-table-column>
      {{else if and (eq $column.IsList "1") (eq $column.HtmlType "datetime")}}
      <el-table-column label="{{$column.ColumnComment}}" align="center" prop="{{$column.HtmlField}}" width="180">
        <template slot-scope="scope">
            <span>{{VueTag "{{"}} parseTime(scope.row.{{$column.HtmlField}}, '{y}-{m}-{d} {h}:{i}:{s}') {{VueTag "}}"}}</span>
        </template>
      </el-table-column>
      {{else if and (eq $column.IsList "1") (eq $column.HtmlType "duration")}}
      <el-table-column label="{{$column.ColumnComment}}" align="center" prop="{{$column.HtmlField}}" width="180">
          <template slot-scope="scope">
              <span>{{VueTag "{{"}} convertDurationString(scope.row.{{$column.HtmlField}}) {{VueTag "}}"}}</span>
          </template>
      </el-table-column>
      {{else if and (eq $column.IsList "1") (eq $column.HtmlType "imagefile")}}
      <el-table-column label="{{$column.ColumnComment}}" align="center" prop="{{$column.HtmlField}}" width="180">
        <template slot-scope="scope">
            <img :src="getUpFileUrl(apiUrl, scope.row.{{$column.HtmlField}})" style="width: 80px;max-height: 80px;"/>
        </template>
      </el-table-column>
      {{else if and (eq $column.IsList "1") (eq $column.HtmlType "enable")}}
      <el-table-column label="{{$column.ColumnComment}}" align="center">
        <template slot-scope="scope">
            <el-switch
              v-model="scope.row.{{$column.HtmlField}}"
              :active-value="1"
              :inactive-value="0"
              @change="{{$column.HtmlField}}Change(scope.row)"
            ></el-switch>
        </template>
      </el-table-column>
      {{else if ne $column.LinkTableName ""}}
      <el-table-column label="{{$column.ColumnComment}}" align="center" prop="{{$column.HtmlField}}" :formatter="{{$column.HtmlField}}Format" width="100">
        <template slot-scope="scope">
          {{VueTag "{{" }} {{$column.HtmlField}}Format(scope.row) {{VueTag "}}" }}
        </template>
      </el-table-column>
      {{else if and (eq $column.IsList "1") (ne $column.DictType "")}}
      <el-table-column label="{{$column.ColumnComment}}" align="center" prop="{{$column.HtmlField}}" :formatter="{{$column.HtmlField}}Format" />
      {{else if and (eq $column.IsList "1") (ne $column.HtmlField "")}}
      <el-table-column label="{{$column.ColumnComment}}" align="center" prop="{{$column.HtmlField}}" />
      {{end}}{{end}}
      <el-table-column label="操作" align="center" class-name="small-padding fixed-width">
        <template slot-scope="scope">
          {{range .table.Actions}}{{if eq .RowSetting "single"}}
          <el-button
            size="mini"
            type="text"
            icon="{{.Icon}}"
            @click="handle{{.Name|CaseCamel}}(scope.row)"
            v-hasPermi="['{{.ModuleName}}/{{$businessName}}/{{.Name}}']"
            {{if .IsValid}}v-show="{{.IsValid|JsExpr "scope.row."}}" {{end}}
          >{{.Abbr}}</el-button>
          {{end}}{{end}}
          <el-button
            size="mini"
            type="text"
            icon="el-icon-edit"
            @click="handleUpdate(scope.row)"
            v-hasPermi="['{{.table.ModuleName}}/{{$businessName}}/edit']"
          >修改</el-button>
          <el-button
            size="mini"
            type="text"
            icon="el-icon-delete"
            @click="handleDelete(scope.row)"
            v-hasPermi="['{{.table.ModuleName}}/{{$businessName}}/delete']"
          >删除</el-button>
        </template>
      </el-table-column>
    </el-table>

    <pagination
      v-show="total>0"
      :total="total"
      :page.sync="queryParams.pageNum"
      :limit.sync="queryParams.pageSize"
      @pagination="getList"
    />

    <!-- 添加或修改{{.table.FunctionName}}对话框 -->
    <el-dialog :title="title" :visible.sync="open" width="800px" append-to-body :close-on-click-modal="false">
      <el-form ref="form" :model="form" :rules="rules" label-width="80px">
      {{range $index, $column := .table.Columns}}
      {{if and (eq $column.IsInsert "1") (ne $column.IsPk "1") (ne $column.HtmlType "underlying")}}
      {{if eq $column.HtmlType "input"}}
      <el-form-item label="{{$column.ColumnComment}}" prop="{{$column.HtmlField}}" {{if $column.IsShow}}v-show="{{$column.IsShow|JsExpr "form."}}" {{end}}>
           <el-input v-model="form.{{$column.HtmlField}}" placeholder="请输入{{$column.ColumnComment}}" />
      </el-form-item>
      {{else if eq $column.HtmlType "number" }}
      <el-form-item label="{{$column.ColumnComment}}" prop="{{$column.HtmlField}}" {{if $column.IsShow}}v-show="{{$column.IsShow|JsExpr "form."}}" {{end}}>
          <el-input-number v-model="form.{{$column.HtmlField}}"></el-input-number>
      </el-form-item>
      {{else if eq $column.HtmlType "select" }}
      {{if ne $column.LinkTableName ""}}
      <el-form-item label="{{$column.ColumnComment}}" prop="{{$column.HtmlField}}" {{if $column.IsShow}}v-show="{{$column.IsShow|JsExpr "form."}}" {{end}}>
          <el-select v-model="form.{{$column.HtmlField}}" placeholder="请选择{{$column.ColumnComment}}">
              <el-option
                  v-for="item in {{$column.HtmlField}}Options"
                  :key="item.key"
                  :label="item.value"
                  :value="item.key"
              ></el-option>
          </el-select>
      </el-form-item>
      {{else if ne $column.DictType ""}}
      <el-form-item label="{{$column.ColumnComment}}" prop="{{$column.HtmlField}}" {{if $column.IsShow}}v-show="{{$column.IsShow|JsExpr "form."}}" {{end}}>
          <el-select v-model="form.{{$column.HtmlField}}" placeholder="请选择{{$column.ColumnComment}}">
              <el-option
                  v-for="dict in {{$column.HtmlField}}Options"
                  :key="dict.key"
                  :label="dict.value"
                  {{if eq $column.GoType "Integer"}}
                  :value="parseInt(dict.key)"
                  {{else}}
                      :value="dict.key"
                  {{end}}
              ></el-option>
          </el-select>
      </el-form-item>
      {{else}}
      <el-form-item label="{{$column.ColumnComment}}" prop="{{$column.HtmlField}}" {{if $column.IsShow}}v-show="{{$column.IsShow|JsExpr "form."}}" {{end}}>
          <el-select v-model="form.{{$column.HtmlField}}" placeholder="请选择{{$column.ColumnComment}}">
              <el-option label="请选择字典生成" value="" />
          </el-select>
      </el-form-item>
      {{end}}
      {{else if eq $column.HtmlType "treeselect" }}
      <el-form-item label="{{$column.ColumnComment}}" prop="{{$column.HtmlField}}" {{if $column.IsShow}}v-show="{{$column.IsShow|JsExpr "form."}}" {{end}}>
        <treeselect v-model="form.{{$column.HtmlField}}" :options="{{$column.HtmlField}}Tree" :normalizer="normalizer" placeholder="请选择所属分类" />
      </el-form-item>
      {{else if eq $column.HtmlType "radio" }}
      {{if ne $column.DictType ""}}
       <el-form-item label="{{$column.ColumnComment}}" prop="{{$column.HtmlField}}" {{if $column.IsShow}}v-show="{{$column.IsShow|JsExpr "form."}}" {{end}}>
           <el-radio-group v-model="form.{{$column.HtmlField}}">
               <el-radio
                v-for="dict in {{$column.HtmlField}}Options"
                :key="dict.key"
                :label="dict.key"
               >{{ VueTag "{{" }}dict.value {{VueTag "}}"}}</el-radio>
           </el-radio-group>
       </el-form-item>
       {{else}}
       <el-form-item label="{{$column.ColumnComment}}" prop="{{$column.HtmlField}}" {{if $column.IsShow}}v-show="{{$column.IsShow|JsExpr "form."}}" {{end}}>
           <el-radio-group v-model="form.{{$column.HtmlField}}">
               <el-radio label="1">请选择字典生成</el-radio>
           </el-radio-group>
       </el-form-item>
       {{end}}
       {{else if eq $column.HtmlType "date"}}
       <el-form-item label="{{$column.ColumnComment}}" prop="{{$column.HtmlField}}" {{if $column.IsShow}}v-show="{{$column.IsShow|JsExpr "form."}}" {{end}}>
           <el-date-picker clearable size="small" style="width: 200px"
               v-model="form.{{$column.HtmlField}}"
               type="date"
               value-format="yyyy-MM-dd HH:mm:ss"
               placeholder="选择{{$column.ColumnComment}}">
           </el-date-picker>
       </el-form-item>
       {{else if eq $column.HtmlType "time"}}
        <el-form-item label="{{$column.ColumnComment}}" prop="{{$column.HtmlField}}" {{if $column.IsShow}}v-show="{{$column.IsShow|JsExpr "form."}}" {{end}}>
          <el-date-picker clearable size="small" style="width: 200px"
              v-model="form.{{$column.HtmlField}}"
              type="time"
              value-format="yyyy-MM-dd HH:mm:ss"
              placeholder="选择{{$column.ColumnComment}}">
          </el-date-picker>
        </el-form-item>
       {{else if eq $column.HtmlType "datetime"}}
       <el-form-item label="{{$column.ColumnComment}}" prop="{{$column.HtmlField}}" {{if $column.IsShow}}v-show="{{$column.IsShow|JsExpr "form."}}" {{end}}>
          <el-date-picker clearable size="small" style="width: 200px"
              v-model="form.{{$column.HtmlField}}"
              type="datetime"
              value-format="yyyy-MM-dd HH:mm:ss"
              placeholder="选择{{$column.ColumnComment}}">
          </el-date-picker>
       </el-form-item>
       {{else if eq $column.HtmlType "duration"}}
        <el-form-item label="{{$column.ColumnComment}}" prop="{{$column.HtmlField}}" {{if $column.IsShow}}v-show="{{$column.IsShow|JsExpr "form."}}" {{end}}>
          <div class="duration">
            <el-input v-model="{{$column.HtmlField}}.hour" @change="{{$column.HtmlField}}Change" type="number" :min="0" >
              <template slot="append">小时</template>
            </el-input>
            <el-input v-model="{{$column.HtmlField}}.minute" @change="{{$column.HtmlField}}Change" type="number" :min="0" >
              <template slot="append">分钟</template>
            </el-input>
            <el-input v-model="{{$column.HtmlField}}.second" @change="{{$column.HtmlField}}Change" type="number" :min="0" >
              <template slot="append">秒</template>
            </el-input>
          </div>
       </el-form-item>
       {{else if eq $column.HtmlType "textarea"}}
       <el-form-item label="{{$column.ColumnComment}}" prop="{{$column.HtmlField}}" {{if $column.IsShow}}v-show="{{$column.IsShow|JsExpr "form."}}" {{end}}>
           <el-input v-model="form.{{$column.HtmlField}}" type="textarea" placeholder="请输入{{$column.ColumnComment}}" />
       </el-form-item>
       {{else if eq $column.HtmlType "checkbox" }}
        <el-form-item label="{{$column.ColumnComment}}" prop="{{$column.HtmlField}}" {{if $column.IsShow}}v-show="{{$column.IsShow|JsExpr "form."}}" {{end}}>
           <el-checkbox-group v-model="form.{{$column.HtmlField}}">
              <el-checkbox
                v-for="dict in {{$column.HtmlField}}Options"
                :key="dict.key"
                :label="dict.key"
              >{{ VueTag "{{" }}dict.value {{VueTag "}}"}}</el-checkbox>
           </el-checkbox-group>
        </el-form-item>
        {{else if eq $column.HtmlType "imagefile"}}
          <el-form-item label="{{$column.ColumnComment}}" prop="{{$column.HtmlField}}" {{if $column.IsShow}}v-show="{{$column.IsShow|JsExpr "form."}}" {{end}}>
            <el-upload
              v-loading="upLoading"
              class="avatar-uploader"
              :action="apiUrl+'/system/upload/upImg'"
              :show-file-list="false"
              name="file"
              :data="setUpData()"
              :on-success="handleAvatarSuccess"
              :before-upload="beforeAvatarUpload"
            >
              <img v-if="{{$column.HtmlField}}Blob" :src="{{$column.HtmlField}}Blob" class="avatar" />
              <i v-else class="el-icon-plus avatar-uploader-icon"></i>
            </el-upload>
          </el-form-item>
       {{else if eq $column.HtmlType "richtext"}}
        <el-form-item label="{{$column.ColumnComment}}" prop="{{$column.HtmlField}}" {{if $column.IsShow}}v-show="{{$column.IsShow|JsExpr "form."}}" {{end}}>
          <Editor v-model="form.{{$column.HtmlField}}" />
        </el-form-item>
       {{end}}
       {{end}}
       {{end}}
      </el-form>
      {{if ne $tabs 0}}
      <el-tabs type="border-card" v-if="form.id">
          {{range $index, $column := .table.Tabs}}
              {{if eq $column.HtmlType "annex"}}
          <el-tab-pane label="{{$column.ColumnComment}}">
              <{{$column.LinkTableClass|CaseCamelLower}} :{{$column.LinkLabelId}}="form.{{$.table.PkColumn.HtmlField}}"></{{$column.LinkTableClass|CaseCamelLower}}>
          </el-tab-pane>
              {{end}}
          {{end}}
      </el-tabs>
      {{end}}
      <div slot="footer" class="dialog-footer">
        <el-button type="primary" @click="submitForm">确 定</el-button>
        <el-button @click="cancel">取 消</el-button>
      </div>
    </el-dialog>
    {{range .table.Actions}}
    <{{.Name|CaseCamel}} ref="{{.Name}}" @success="getList()"></{{.Name|CaseCamel}}>
    {{end}}
  </div>
</template>

<script>


{{$plugin:=""}}
{{if ContainsI $.table.PackageName "plugins"}}
{{$plugin = "plugins/"}}
{{end}}


import {
    list{{.table.ClassName}},
    get{{.table.ClassName}},
    del{{.table.ClassName}},
    add{{.table.ClassName}},
    update{{.table.ClassName}},
    {{range $index,$column:= .table.Columns}}
    {{if and (eq $column.HtmlType "enable") (eq $column.IsList "1") }}
    change{{$.table.ClassName}}{{$column.GoField}},
    {{end}}
    {{if and (ne $column.LinkTableName "") (ne $column.LinkTableName $.table.TableName)}}
    list{{$column.HtmlField|CaseCamel}},
    {{end}}
    {{end}}
} from "@/api/{{$plugin}}{{.table.ModuleName}}/{{$businessName}}";
{{if $hasImageFile}}
import { getToken } from "@/utils/auth";
{{end}}
{{if $hasRichText}}
import Editor from '@/components/Editor';
{{end}}
{{if $hasTreeSelect}}
import Treeselect from "@riophae/vue-treeselect";
import "@riophae/vue-treeselect/dist/vue-treeselect.css";
{{end}}
{{range $index, $column := .table.Tabs}}
  {{if eq $column.HtmlType "annex"}}
import {{$column.LinkTableClass|CaseCamelLower}} from "../../../{{$column.LinkTableModule}}/{{$column.LinkTableClass|CaseCamelLower}}/list/index.vue";
  {{end}}
{{end}}
{{range .table.Actions}}
import {{.Name|CaseCamel}} from '../dialog/{{.Name}}.vue';
{{end}}

export default {
  components:{
    {{if $hasRichText}}Editor,{{end}}
    {{if $hasTreeSelect}}Treeselect,{{end}}
    {{range $index, $column := .table.Tabs}}
    {{if eq $column.HtmlType "annex"}}{{$column.LinkTableClass|CaseCamelLower}},{{end}}
    {{end}}
    {{range .table.Actions}}
    {{.Name|CaseCamel}},
    {{end}}
  },
  name: "{{.table.ClassName}}",
  props: [
      {{range $index, $column := .table.Columns}}
      {{if eq $column.HtmlType "underlying"}}
      "{{$column.HtmlField}}",
      {{end}}
      {{end}}
  ],
  data() {
    return {
      // 遮罩层
      loading: true,
      // 选中数组
      ids: [],
      // 非单个禁用
      single: true,
      // 非多个禁用
      multiple: true,
      // 总条数
      total: 0,
      // {{.table.FunctionName}}表格数据
      {{$businessName}}List: [],
      // 弹出层标题
      title: "",
      // 是否显示弹出层
      open: false,
      {{range $index, $column := .table.Columns}}
      {{if ne $column.DictType ""}}
      // {{$column.HtmlField}}Options字典数据
      {{$column.HtmlField}}Options: [],
      {{else if ne $column.LinkTableName ""}}
      // {{$column.HtmlField}}Options关联表数据
      {{$column.HtmlField}}Options: [],
      {{end}}
      {{if eq $column.HtmlType "duration"}}
      {{$column.HtmlField}}: {
          hour: 0,
          minute: 0,
          second: 0
      },
      {{end}}
      {{if eq $column.HtmlType "treeselect"}}
      // {{$column.HtmlField}}Tree关联表数据
      {{$column.HtmlField}}Tree: [],
      {{end}}
      {{if eq $column.HtmlType "imagefile"}}
      // 图片上传地址
      {{$column.HtmlField}}Blob: "",
      // 上传加载
      upLoading: false,
      {{end}}
      {{end}}
      // 查询参数
      queryParams: {
        pageNum: 1,
        pageSize: 10,{{range $index, $column := .table.Columns}}{{if eq $column.IsQuery "1"}}
        {{$column.HtmlField}}: undefined{{if ne $lens $index}},{{end}}{{end}}{{end}}
      },
      // 表单参数
      form: {},
      // 表单校验
      rules: { {{range $index, $column := .table.Columns}}{{if and (eq $column.IsRequired "1") (ne $column.HtmlType "underlying")}}
        {{$column.HtmlField}} : [
          { required: true, message: "{{$column.ColumnComment}}不能为空", trigger: "blur" }
        ]{{if ne $lens $index}},{{end}}{{end}}{{end}}
      }
    };
  },
  created() {
    {{range $index, $column := .table.Columns}}
    {{if ne $column.DictType ""}}
    this.getDicts("{{$column.DictType}}").then(response => {
      this.{{$column.HtmlField}}Options = response.data.values||[];
    });
    {{else if ne $column.LinkTableName ""}}
    this.get{{$column.HtmlField|CaseCamel}}Items()
    {{end}}
    {{end}}

    this.getList();
  },
  methods: {
    {{$setUpData:=true}}
    {{range $index, $column := .table.Columns}}
    {{if ne $column.LinkTableName ""}}
    //关联{{$column.LinkTableName}}表选项
    get{{$column.HtmlField|CaseCamel}}Items() {
      this.getItems(list{{$column.HtmlField|CaseCamel}}, {pageSize:10000}).then(res => {
        this.{{$column.HtmlField}}Options = this.setItems(res, '{{$column.LinkLabelId}}', '{{$column.LinkLabelName| CaseCamelLower}}')
      })
    },
    {{else if and (eq $column.HtmlType "enable") (eq $column.IsList "1")}}
    // {{$column.ColumnComment}}修改
    {{$column.HtmlField}}Change(row) {
      let text = row.{{$column.HtmlField}} === 1 ? "启用" : "停用";
      this.$confirm('确认要"' + text + '"吗?', "警告", {
          confirmButtonText: "确定",
          cancelButtonText: "取消",
          type: "warning"
        }).then(function() {
          return change{{$.table.ClassName}}{{$column.GoField}}(row.{{$.table.PkColumn.HtmlField}}, row.{{$column.HtmlField}});
        }).then(() => {
          this.msgSuccess(text + "成功");
        }).catch(function() {
          row.userStatus =row.userStatus === 0 ?1 : 0;
        });
    },
    {{end}}
    {{end}}
    /** 查询{{.table.FunctionName}}列表 */
    getList() {
      this.loading = true;
      {{range $index, $column := .table.Columns}}
      {{if eq $column.HtmlType "underlying"}}
      this.queryParams.{{$column.HtmlField}} = this.{{$column.HtmlField}};
      {{end}}
      {{end}}
      list{{.table.ClassName}}(this.queryParams).then(response => {
        this.{{$businessName}}List = response.data.list;
        this.total = response.data.total;
        this.loading = false;
      });
    },
    {{range $index, $column := .table.Columns}}
    {{if ne $column.DictType ""}}
    {{if eq $column.HtmlType "checkbox"}}
    // {{$column.ColumnComment}}字典翻译
    {{$column.HtmlField}}Format(row, column) {
      {{if eq $column.GoType "int"}}
      function bitcode(v){
          let res = []
          if(v&1){
            res.push("1")
          }
          if(v&2){
            res.push("2")
          }
          return res
        }
        let {{$column.HtmlField}} = bitcode(row.{{$column.HtmlField}})
      {{else}}
      let {{$column.HtmlField}} = row.{{$column.HtmlField}}.split(",")
      {{end}}
      let data = [];
      {{$column.HtmlField}}.map(item=>{
        data.push(this.selectDictLabel(this.{{$column.HtmlField}}Options, item))
      })
      return data.join(",")
    },
    {{else}}
    // {{$column.ColumnComment}}字典翻译
    {{$column.HtmlField}}Format(row, column) {
      return this.selectDictLabel(this.{{$column.HtmlField}}Options, row.{{$column.HtmlField}});
    },
    {{end}}
    {{else if ne $column.LinkTableName ""}}
    // {{$column.ColumnComment}}关联表翻译
    {{$column.HtmlField}}Format(row, column) {
      return this.selectItemsLabel(this.{{$column.HtmlField}}Options, row.{{$column.HtmlField}});
    },
    {{end}}
    {{if eq $column.HtmlType "duration"}}
    {{$column.HtmlField}}Change(){
      this.form.{{$column.HtmlField}} = this.convertDuration(this.{{$column.HtmlField}})
    },
    {{end}}
    {{if eq $column.HtmlType "imagefile"}}
    handleAvatarSuccess(res, file) {
      if (res.code === 0) {
        this.{{$column.HtmlField}}Blob = URL.createObjectURL(file.raw);
        this.form.{{$column.HtmlField}} = res.data.fileInfo.fileUrl;
      } else {
        this.msgError(res.msg);
      }
      this.upLoading = false;
    },
    beforeAvatarUpload(file) {
      this.upLoading = true;
      return true;
    },
    setUpData() {
      return { token: getToken() };
    },
    {{end}}
    {{end}}
    {{if $hasDuration}}
    // 将时长(对象)转换成时长(秒)
    convertDuration(v){
      return parseInt(v.hour) * 3600 + parseInt(v.minute) * 60 + parseInt(v.second)
    },
    // 将时长(秒)转换成时长(对象)
    convertDurationObject(v) {
      let hour = Math.floor(v/3600)
      let minute = Math.floor(v/60) - hour * 60
      let second = Math.floor(v) - hour * 3600 - minute * 60
      return {
        hour:hour,
        minute:minute,
        second:second,
      }
    },
    // 将时长(秒)转换成时长(字符串)
    convertDurationString(v){
      let hour = Math.floor(v/3600)
      let minute = Math.floor(v/60) - hour * 60
      let second = Math.floor(v) - hour * 3600 - minute * 60
      let str = ""
      if(hour){
        str+=hour+"小时"
      }
      if(minute){
        str+=minute+"分钟"
      }
      if(second){
        str+=second+"秒"
      }
      return str
    },
    {{end}}
    {{if $hasTreeSelect}}
    /** 转换树状数据结构 */
    normalizer(node) {
      if (node.children && !node.children.length) {
        delete node.children;
      }
      return {
        id: node.id,
        label: node.name,
        children: node.children
      };
    },
    /** 查询树状结构 */
    getTreeselect() {
      {{range $index, $column := .table.Columns}}
      {{if eq $column.HtmlType "treeselect"}}
      list{{$column.HtmlField|CaseCamel}}(this.queryParams).then(response => {
        this.{{$column.HtmlField}}Tree = [];
        const data = { id: 0, name: '顶级节点', children: [] };
        data.children = this.handleTree(response.data.list||[], "id", "parentId");
        this.{{$column.HtmlField}}Tree.push(data);
      });
      {{end}}
      {{end}}
    },
    {{end}}
    // 取消按钮
    cancel() {
      this.open = false;
      this.reset();
    },
    // 表单重置
    reset() {
      this.form = {
        {{range $index, $column := .table.Columns}}
        {{if eq $column.HtmlType "radio"}}
        {{$column.HtmlField}}: "0" ,
        {{else if eq $column.HtmlType "checkbox"}}
        {{$column.HtmlField}}: [] ,
        {{else}}
        {{$column.HtmlField}}: undefined,
        {{end}}
        {{end}}
      };
      {{range $index, $column := .table.Columns}}
      {{if eq $column.HtmlType "imagefile"}}
      this.{{$column.HtmlField}}Blob = ''
      {{end}}
      {{end}}
      this.resetForm("form");
    },
    /** 搜索按钮操作 */
    handleQuery() {
      this.queryParams.pageNum = 1;
      this.getList();
    },
    /** 重置按钮操作 */
    resetQuery() {
      this.resetForm("queryForm");
      this.handleQuery();
    },
    // 多选框选中数据
    handleSelectionChange(selection) {
      this.ids = selection.map(item => item.{{.table.PkColumn.HtmlField}})
      this.single = selection.length!=1
      this.multiple = !selection.length
    },
    /** 新增按钮操作 */
    handleAdd() {
      this.reset();
      {{if $hasTreeSelect}}
      this.getTreeselect();
      {{end}}
      this.open = true;
      this.title = "添加{{.table.FunctionName}}";
    },
    {{range .table.Actions}}
        {{if eq .RowSetting ""}}
    handle{{.Name|CaseCamel}}() {
        this.$refs.{{.Name}}.show({})
    },
        {{else}}
    handle{{.Name|CaseCamel}}(row) {
        this.$refs.{{.Name}}.show(row)
    },
        {{end}}
    {{end}}
    /** 修改按钮操作 */
    handleUpdate(row) {
      this.reset();
      {{if $hasTreeSelect}}
      this.getTreeselect();
      {{end}}
      const {{.table.PkColumn.HtmlField}} = row.{{.table.PkColumn.HtmlField}} || this.ids
      get{{.table.ClassName}}({{.table.PkColumn.HtmlField}}).then(response => {
        let data = response.data;
        {{range $index, $column := .table.Columns}}
        {{if eq $column.HtmlType "checkbox"}}
        {{if eq $column.GoType "int"}}
          function bitcode(v){
              let res = []
              if(v&1){
                res.push("1")
              }
              if(v&2){
                res.push("2")
              }
              return res
            }
            data.{{$column.HtmlField}} = bitcode(data.{{$column.HtmlField}})
        {{else}}
        data.{{$column.HtmlField}} = data.{{$column.HtmlField}}.split(",")
        {{end}}
        {{else if eq $column.HtmlType "radio" "select"}}
        data.{{$column.HtmlField}} = ''+data.{{$column.HtmlField}}
        {{else if eq $column.HtmlType "imagefile"}}
        this.{{$column.HtmlField}}Blob = this.getUpFileUrl(this.apiUrl, data.{{$column.HtmlField}});
        {{end}}
        {{end}}
        this.form = data;
        this.open = true;
        this.title = "修改{{.table.FunctionName}}";
      });
    },
    /** 提交按钮 */
    submitForm: function() {
      this.$refs["form"].validate(valid => {
        if (valid) {
          {{range $index, $column := .table.Columns}}
          {{if eq $column.HtmlType "underlying"}}
          this.form.{{$column.HtmlField}} = this.{{$column.HtmlField}};
          {{end}}
          {{end}}
          if (this.form.{{.table.PkColumn.HtmlField}} != undefined) {
            update{{.table.ClassName}}(this.form).then(response => {
              if (response.code === 0) {
                this.msgSuccess("修改成功");
                this.open = false;
                this.getList();
              } else {
                this.msgError(response.msg);
              }
            });
          } else {
            add{{.table.ClassName}}(this.form).then(response => {
              if (response.code === 0) {
                this.msgSuccess("新增成功");
                this.open = false;
                this.getList();
              } else {
                this.msgError(response.msg);
              }
            });
          }
        }
      });
    },
    /** 删除按钮操作 */
    handleDelete(row) {
      const {{.table.PkColumn.HtmlField}}s = row.{{.table.PkColumn.HtmlField}} || this.ids;
      this.$confirm('是否确认删除{{.table.FunctionName}}编号为"' + {{.table.PkColumn.HtmlField}}s + '"的数据项?', "警告", {
          confirmButtonText: "确定",
          cancelButtonText: "取消",
          type: "warning"
        }).then(function() {
          return del{{.table.ClassName}}({{.table.PkColumn.HtmlField}}s);
        }).then(() => {
          this.getList();
          this.msgSuccess("删除成功");
        }).catch(function() {});
    }
  }
};
</script>

<style>
{{if $hasDuration}}
.duration .el-input {
  width: 130px;
}
{{end}}

{{if $hasImageFile}}
.avatar-uploader .el-upload {
  border: 1px dashed #d9d9d9;
  border-radius: 6px;
  cursor: pointer;
  position: relative;
  overflow: hidden;
}
.avatar-uploader .el-upload:hover {
  border-color: #409eff;
}
.avatar-uploader-icon {
  font-size: 28px;
  color: #8c939d;
  width: 178px;
  height: 178px;
  line-height: 178px;
  text-align: center;
}
.avatar {
  width: 178px;
  height: 178px;
  display: block;
}
{{end}}

{{if $hasRichText}}
.quill-file {
  height: 0;
}

.quill-editor {
  height: auto !important;
}
{{end}}
</style>