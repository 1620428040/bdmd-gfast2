// ==========================================================================
// 自动生成代码，无需手动修改，重新生成会自动覆盖.
// 数据:parse.Model
// ==========================================================================
package model

import "github.com/gogf/gf/util/gmeta"






type User struct {
    gmeta.Meta `orm:"table:sys_user"`
    // id integer id
    Id int `orm:"id,primary" json:"id"`
    // user_nickname string 昵称
    UserNickname string `orm:"user_nickname" json:"userNickname"`
}

func (v *User) Compose() {
}
