// ==========================================================================
// 自动生成代码，无需手动修改，重新生成会自动覆盖.
// 数据:parse.Model
// ==========================================================================
package model

import "github.com/gogf/gf/util/gmeta"






type Dept struct {
    gmeta.Meta `orm:"table:sys_dept"`
    // dept_id integer dept_id
    DeptId int `orm:"dept_id,primary" json:"deptId"`
    // dept_name string 部门名称
    DeptName string `orm:"dept_name" json:"deptName"`
    // parent_id tree 上级
    ParentId int `orm:"parent_id" json:"parentId"`
}

func (v *Dept) Compose() {
}
