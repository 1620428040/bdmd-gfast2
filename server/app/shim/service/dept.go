// ==========================================================================
// GFast自动生成业务逻辑层相关代码，只生成一次，按需修改,再次生成不会覆盖.
// 生成日期：2023-12-04 16:34:15
// 生成路径: gfast/app/shim/service/dept.go
// 生成人：guo
// ==========================================================================


package service
import (
    "context"
	comModel "gfast/app/common/model"
	"gfast/app/shim/dao"
	"gfast/app/shim/model"	
	"github.com/gogf/gf/errors/gerror"
	"github.com/gogf/gf/frame/g"
)
type dept struct {
}
type IDept interface {
	GetList(req *dao.DeptSearchReq) (total, page int, list []*model.Dept, err error)
	GetInfoById(ctx context.Context,id int) (info *model.Dept, err error)
	Create(ctx context.Context,req *dao.DeptAddReq) (id int64, err error)
    Edit(ctx context.Context,req *dao.DeptEditReq) error
    DeleteByIds(ctx context.Context,ids []int) (err error)
}
var Dept IDept = new(dept)
// GetList 获取任务列表
func (s *dept) GetList(req *dao.DeptSearchReq) (total, page int, list []*model.Dept, err error) {
	m := dao.Dept.Ctx(req.Ctx)
	total, err = m.Count()
	if err != nil {
		g.Log().Error(err)
		err = gerror.New("获取总行数失败")
		return
	}    
    if req.PageNum == 0 {
        req.PageNum = 1
    }
    page = req.PageNum
    if req.PageSize == 0 {
        req.PageSize = comModel.PageSize
    }
    order:= "dept_id asc"
    if req.OrderBy!=""{
        order = req.OrderBy
    }
    err = m.Page(page, req.PageSize).Order(order).WithAll().Scan(&list)    
	if err != nil {
		g.Log().Error(err)
		err = gerror.New("获取数据失败")
	}
	for _, v := range list {
        v.Compose()
    }
	return
}
// GetInfoById 通过id获取
func (s *dept) GetInfoById(ctx context.Context,id int) (info *model.Dept, err error) {
	if id == 0 {
		err = gerror.New("参数错误")
		return
	}
	err = dao.Dept.Ctx(ctx).Where(dao.Dept.Columns.DeptId, id).WithAll().Scan(&info)
	if err != nil {
		g.Log().Error(err)
	}
	if info == nil || err != nil {
		err = gerror.New("获取信息失败")
	}
	info.Compose()
	return
}
// Create 添加
func (s *dept) Create(ctx context.Context,req *dao.DeptAddReq) (id int64, err error) {    
	return dao.Dept.Ctx(ctx).InsertAndGetId(req)
}
// Edit 修改
func (s *dept) Edit(ctx context.Context,req *dao.DeptEditReq) error {    
	_, err := dao.Dept.Ctx(ctx).FieldsEx(dao.Dept.Columns.DeptId).Where(dao.Dept.Columns.DeptId, req.DeptId).
		Update(req)
	return err
}
// DeleteByIds 删除
func (s *dept) DeleteByIds(ctx context.Context,ids []int) (err error) {
	if len(ids) == 0 {
		err = gerror.New("参数错误")
		return
	}    
	_, err = dao.Dept.Ctx(ctx).Delete(dao.Dept.Columns.DeptId+" in (?)", ids)
	if err != nil {
		g.Log().Error(err)
		err = gerror.New("删除失败")
	}
	return
}
