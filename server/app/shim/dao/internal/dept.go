// ==========================================================================
// GFast自动生成dao internal操作代码，无需手动修改，重新生成会自动覆盖.
// 生成日期：2023-12-06 17:31:10
// 生成路径: gfast/app/shim/dao/internal/sys_dept.go
// 生成人：guo
// ==========================================================================


package internal
import (
    "context"
    "github.com/gogf/gf/database/gdb"
    "github.com/gogf/gf/frame/g"
)
// DeptDao is the manager for logic model data accessing and custom defined data operations functions management.
type DeptDao struct {
    Table   string         // Table is the underlying table name of the DAO.
    Group   string         // Group is the database configuration group name of current DAO.
    Columns DeptColumns // Columns is the short type for Columns, which contains all the column names of Table for convenient usage.
}
// DeptColumns defines and stores column names for table sys_dept.
type DeptColumns struct {    
    DeptId  string  // dept_id    
    DeptName  string  // 部门名称    
    ParentId  string  // 上级    
}
var deptColumns = DeptColumns{    
    DeptId:  "dept_id",    
    DeptName:  "dept_name",    
    ParentId:  "parent_id",    
}
// NewDeptDao creates and returns a new DAO object for table data access.
func NewDeptDao() *DeptDao {
	return &DeptDao{
        Group:    "default",
        Table: "sys_dept",
        Columns:deptColumns,
	}
}
// DB retrieves and returns the underlying raw database management object of current DAO.
func (dao *DeptDao) DB() gdb.DB {
	return g.DB(dao.Group)
}
// Ctx creates and returns the Model for current DAO, It automatically sets the context for current operation.
func (dao *DeptDao) Ctx(ctx context.Context) *gdb.Model {
	return dao.DB().Model(dao.Table).Safe().Ctx(ctx)
}
// Transaction wraps the transaction logic using function f.
// It rollbacks the transaction and returns the error from function f if it returns non-nil error.
// It commits the transaction and returns nil if function f returns nil.
//
// Note that, you should not Commit or Rollback the transaction in function f
// as it is automatically handled by this function.
func (dao *DeptDao) Transaction(ctx context.Context, f func(ctx context.Context, tx *gdb.TX) error) (err error) {
	return dao.Ctx(ctx).Transaction(ctx, f)
}