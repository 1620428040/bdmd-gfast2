// ==========================================================================
// GFast自动生成dao操作代码，无需手动修改，重新生成不会自动覆盖.
// 生成日期：2023-12-04 16:34:15
// 生成路径: gfast/app/shim/dao/sys_dept.go
// 生成人：guo
// ==========================================================================


package dao
import (
    comModel "gfast/app/common/model"
    "gfast/app/shim/dao/internal"    
)
// deptDao is the manager for logic model data accessing and custom defined data operations functions management.
// You can define custom methods on it to extend its functionality as you wish.
type deptDao struct {
	*internal.DeptDao
}
var (
    // Dept is globally public accessible object for table tools_gen_table operations.
    Dept = deptDao{
        internal.NewDeptDao(),
    }
)


// Fill with you ideas below.


// DeptSearchReq 分页请求参数
type DeptSearchReq struct {    
    comModel.PageReq
}
// DeptAddReq 添加操作请求参数
type DeptAddReq struct {    
    DeptName  string   `p:"deptName" `    
    ParentIdDeptId  int   `p:"parentIdDeptId" `    
}
// DeptEditReq 修改操作请求参数
type DeptEditReq struct {
    DeptId    int  `p:"deptId" v:"required#主键ID不能为空"`    
    DeptName  string `p:"deptName" `    
    ParentIdDeptId  int `p:"parentIdDeptId" `    
}
