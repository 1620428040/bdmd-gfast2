// ==========================================================================
// GFast自动生成dao操作代码，无需手动修改，重新生成不会自动覆盖.
// 生成日期：2023-12-04 16:34:14
// 生成路径: gfast/app/shim/dao/sys_user.go
// 生成人：guo
// ==========================================================================


package dao
import (
    comModel "gfast/app/common/model"
    "gfast/app/shim/dao/internal"    
)
// userDao is the manager for logic model data accessing and custom defined data operations functions management.
// You can define custom methods on it to extend its functionality as you wish.
type userDao struct {
	*internal.UserDao
}
var (
    // User is globally public accessible object for table tools_gen_table operations.
    User = userDao{
        internal.NewUserDao(),
    }
)


// Fill with you ideas below.


// UserSearchReq 分页请求参数
type UserSearchReq struct {    
    comModel.PageReq
}
// UserAddReq 添加操作请求参数
type UserAddReq struct {    
    UserNickname  string   `p:"userNickname" `    
}
// UserEditReq 修改操作请求参数
type UserEditReq struct {
    Id    int  `p:"id" v:"required#主键ID不能为空"`    
    UserNickname  string `p:"userNickname" `    
}
