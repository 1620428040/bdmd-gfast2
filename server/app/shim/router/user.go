// ==========================================================================
// GFast自动生成路由代码，只生成一次，按需修改,再次生成不会覆盖.
// 生成日期：2023-12-04 16:34:14
// 生成路径: gfast/app/shim/router/user.go
// 生成人：guo
// ==========================================================================


package router
import (
    "gfast/app/shim/api"
    "gfast/middleware"
    "github.com/gogf/gf/frame/g"
    "github.com/gogf/gf/net/ghttp"    
    sysApi "gfast/app/system/api"    
)
//加载路由
func init() {
    s := g.Server()
    s.Group("/", func(group *ghttp.RouterGroup) {
        group.Group("/shim", func(group *ghttp.RouterGroup) {
            group.Group("/user", func(group *ghttp.RouterGroup) {
                //gToken拦截器                
                sysApi.GfToken.AuthMiddleware(group)                
                //context拦截器
                group.Middleware(middleware.Ctx, middleware.Auth)                
                //后台操作日志记录
                group.Hook("/*", ghttp.HookAfterOutput, sysApi.SysOperLog.OperationLog)                
                group.GET("list", api.User.List)
                group.GET("get", api.User.Get)
                group.POST("create", api.User.Create)
                group.PUT("edit", api.User.Edit)
                group.DELETE("delete", api.User.Delete)                
            })
        })
    })
}
