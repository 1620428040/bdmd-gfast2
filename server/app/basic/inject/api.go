package inject

import (
	"gfast/app/basic/model"
	sysApi "gfast/app/system/api"
	"github.com/gogf/gf/frame/g"
	"github.com/gogf/gf/net/ghttp"
)

// 加载路由
func init() {
	s := g.Server()
	s.Group("/", func(group *ghttp.RouterGroup) {
		//gToken拦截器
		//sysApi.GfToken.AuthMiddleware(group)
		//context拦截器
		//group.Middleware(middleware.Ctx, middleware.Auth)

		var c = new(sysApi.SystemBase)
		group.POST("/code/gen", func(r *ghttp.Request) {
			req := model.CodeRule{}
			if err := r.Parse(&req); err != nil {
				c.FailJsonExit(r, err.Error())
			}
			code, err := GenCode(r.GetCtx(), req.Key)
			if err != nil {
				c.FailJsonExit(r, err.Error())
			}
			c.SusJsonExit(r, code)
		})
		// multi 类型的字段，选中若干条记录加入到列表中
		group.PATCH("/material/procedures/selected", func(r *ghttp.Request) {
			id := r.GetInt("id")
			list := r.GetInts("list")
			err := MultiSelectedMaterialProcedures(r.GetCtx(), id, list)
			if err != nil {
				c.FailJsonExit(r, err.Error())
			}
			c.SusJsonExit(r, "添加成功")
		})
		// 复制物料
		group.POST("/material/copy", func(r *ghttp.Request) {
			var req *model.Material
			if err := r.Parse(&req); err != nil {
				c.FailJsonExit(r, err.Error())
			}
			err := MaterialCopy(r.GetCtx(), req)
			if err != nil {
				c.FailJsonExit(r, err.Error())
			}
			c.SusJsonExit(r, "复制成功")
		})
		// 获取物料的bom树
		group.GET("/bom/tree", func(r *ghttp.Request) {
			req := model.Material{}
			if err := r.Parse(&req); err != nil {
				c.FailJsonExit(r, err.Error())
			}
			root, err := GetMaterialBomTree(r.GetCtx(), req.Id)
			if err != nil {
				c.FailJsonExit(r, err.Error())
			}
			c.SusJsonExit(r, root)
		})
		// 获取物料的物料统计
		group.GET("/bom/data", func(r *ghttp.Request) {
			req := model.Material{}
			if err := r.Parse(&req); err != nil {
				c.FailJsonExit(r, err.Error())
			}
			root, err := GetMaterialBomTree(r.GetCtx(), req.Id)
			if err != nil {
				c.FailJsonExit(r, err.Error())
			}
			count := Merge(root)
			c.SusJsonExit(r, map[string]interface{}{
				"tree":  root,
				"count": count,
			})
		})
		// 获取产品的选配表
		group.POST("/product/routing/preview", func(r *ghttp.Request) {
			id := r.GetInt("id")
			data := make([]Optional, 0)
			if err := r.Parse(&data); err != nil {
				c.FailJsonExit(r, err.Error())
			}
			res, err := ListOptionalMaterials(r.GetCtx(), id, data)
			if err != nil {
				c.FailJsonExit(r, err.Error())
			}
			c.SusJsonExit(r, map[string]interface{}{
				"params": res,
			})
		})
		// 获取产品的bom树和物料统计
		group.POST("/product/bom", func(r *ghttp.Request) {
			id := r.GetInt("id")
			params := make([]Optional, 0)
			if err := r.Parse(&params); err != nil {
				c.FailJsonExit(r, err.Error())
			}
			tree, params, err := GetProductBomTree(r.GetCtx(), id, params)
			if err != nil {
				c.FailJsonExit(r, err.Error())
			}
			c.SusJsonExit(r, g.Map{
				"tree":   tree,
				"params": params,
				"stat":   StatBomTreeMaterial(tree, 1),
			})
		})
		// 生成产品的工艺路线？
		group.GET("/product/genRouting/options", func(r *ghttp.Request) {
			//id := r.GetInt("id")
			//res, err := GenRoutingOptions(r.GetCtx(), id)
			//if err != nil {
			//	c.FailJsonExit(r, err.Error())
			//}
			//c.SusJsonExit(r, res)
		})
		group.GET("/genRouting", func(r *ghttp.Request) {
			req := model.Material{}
			if err := r.Parse(&req); err != nil {
				c.FailJsonExit(r, err.Error())
			}
			res, err := GenRouting(r.GetCtx(), req.Id)
			if err != nil {
				c.FailJsonExit(r, err.Error())
			}
			c.SusJsonExit(r, res)
		})
	})
}
