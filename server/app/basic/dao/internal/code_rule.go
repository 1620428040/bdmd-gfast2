// ==========================================================================
// GFast自动生成dao internal操作代码，无需手动修改，重新生成会自动覆盖.
// 生成日期：2023-11-06 14:26:53
// 生成路径: gfast/app/basic/dao/internal/m_code_rule.go
// 生成人：guo
// ==========================================================================


package internal
import (
    "context"
    "github.com/gogf/gf/database/gdb"
    "github.com/gogf/gf/frame/g"
)
// CodeRuleDao is the manager for logic model data accessing and custom defined data operations functions management.
type CodeRuleDao struct {
    Table   string         // Table is the underlying table name of the DAO.
    Group   string         // Group is the database configuration group name of current DAO.
    Columns CodeRuleColumns // Columns is the short type for Columns, which contains all the column names of Table for convenient usage.
}
// CodeRuleColumns defines and stores column names for table m_code_rule.
type CodeRuleColumns struct {    
    Id  string  // id    
    Key  string  // 类型    
    Name  string  // 名称    
    Prefix  string  // 前缀    
    Time  string  // 时间格式    
    SerialBegin  string  // 流水号起点    
    SerialLength  string  // 流水号长度    
    SerialCounter  string  // 当前流水号    
}
var codeRuleColumns = CodeRuleColumns{    
    Id:  "id",    
    Key:  "key",    
    Name:  "name",    
    Prefix:  "prefix",    
    Time:  "time",    
    SerialBegin:  "serial_begin",    
    SerialLength:  "serial_length",    
    SerialCounter:  "serial_counter",    
}
// NewCodeRuleDao creates and returns a new DAO object for table data access.
func NewCodeRuleDao() *CodeRuleDao {
	return &CodeRuleDao{
        Group:    "default",
        Table: "m_code_rule",
        Columns:codeRuleColumns,
	}
}
// DB retrieves and returns the underlying raw database management object of current DAO.
func (dao *CodeRuleDao) DB() gdb.DB {
	return g.DB(dao.Group)
}
// Ctx creates and returns the Model for current DAO, It automatically sets the context for current operation.
func (dao *CodeRuleDao) Ctx(ctx context.Context) *gdb.Model {
	return dao.DB().Model(dao.Table).Safe().Ctx(ctx)
}
// Transaction wraps the transaction logic using function f.
// It rollbacks the transaction and returns the error from function f if it returns non-nil error.
// It commits the transaction and returns nil if function f returns nil.
//
// Note that, you should not Commit or Rollback the transaction in function f
// as it is automatically handled by this function.
func (dao *CodeRuleDao) Transaction(ctx context.Context, f func(ctx context.Context, tx *gdb.TX) error) (err error) {
	return dao.Ctx(ctx).Transaction(ctx, f)
}