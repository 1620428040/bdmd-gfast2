// ==========================================================================
// GFast自动生成dao操作代码，无需手动修改，重新生成不会自动覆盖.
// 生成日期：2023-11-06 13:26:05
// 生成路径: gfast/app/basic/dao/m_code_rule.go
// 生成人：guo
// ==========================================================================


package dao
import (
    comModel "gfast/app/common/model"
    "gfast/app/basic/dao/internal"    
)
// codeRuleDao is the manager for logic model data accessing and custom defined data operations functions management.
// You can define custom methods on it to extend its functionality as you wish.
type codeRuleDao struct {
	*internal.CodeRuleDao
}
var (
    // CodeRule is globally public accessible object for table tools_gen_table operations.
    CodeRule = codeRuleDao{
        internal.NewCodeRuleDao(),
    }
)


// Fill with you ideas below.


// CodeRuleSearchReq 分页请求参数
type CodeRuleSearchReq struct {    
    comModel.PageReq
}
// CodeRuleAddReq 添加操作请求参数
type CodeRuleAddReq struct {    
    Key  string   `p:"key" `    
    Name  string   `p:"name" `    
    Prefix  string   `p:"prefix" `    
    Time  string   `p:"time" `    
    SerialBegin  int   `p:"serialBegin" `    
    SerialLength  int   `p:"serialLength" `    
    SerialCounter  int   `p:"serialCounter" `    
}
// CodeRuleEditReq 修改操作请求参数
type CodeRuleEditReq struct {
    Id    int  `p:"id" v:"required#主键ID不能为空"`    
    Key  string `p:"key" `    
    Name  string `p:"name" `    
    Prefix  string `p:"prefix" `    
    Time  string `p:"time" `    
    SerialBegin  int `p:"serialBegin" `    
    SerialLength  int `p:"serialLength" `    
    SerialCounter  int `p:"serialCounter" `    
}
