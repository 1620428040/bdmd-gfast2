// ==========================================================================
// GFast自动生成业务逻辑层相关代码，只生成一次，按需修改,再次生成不会覆盖.
// 生成日期：2023-11-06 13:26:05
// 生成路径: gfast/app/basic/service/code_rule.go
// 生成人：guo
// ==========================================================================


package service
import (
    "context"
	comModel "gfast/app/common/model"
	"gfast/app/basic/dao"
	"gfast/app/basic/model"	
	"github.com/gogf/gf/errors/gerror"
	"github.com/gogf/gf/frame/g"
)
type codeRule struct {
}
type ICodeRule interface {
	GetList(req *dao.CodeRuleSearchReq) (total, page int, list []*model.CodeRule, err error)
	GetInfoById(ctx context.Context,id int) (info *model.CodeRule, err error)
	Create(ctx context.Context,req *dao.CodeRuleAddReq) (id int64, err error)
    Add(ctx context.Context,req *dao.CodeRuleAddReq) (err error)
    Edit(ctx context.Context,req *dao.CodeRuleEditReq) error
    DeleteByIds(ctx context.Context,ids []int) (err error)
}
var CodeRule ICodeRule = new(codeRule)
// GetList 获取任务列表
func (s *codeRule) GetList(req *dao.CodeRuleSearchReq) (total, page int, list []*model.CodeRule, err error) {
	m := dao.CodeRule.Ctx(req.Ctx)
	total, err = m.Count()
	if err != nil {
		g.Log().Error(err)
		err = gerror.New("获取总行数失败")
		return
	}    
    if req.PageNum == 0 {
        req.PageNum = 1
    }
    page = req.PageNum
    if req.PageSize == 0 {
        req.PageSize = comModel.PageSize
    }
    order:= "id asc"
    if req.OrderBy!=""{
        order = req.OrderBy
    }
    err = m.Page(page, req.PageSize).Order(order).WithAll().Scan(&list)    
	if err != nil {
		g.Log().Error(err)
		err = gerror.New("获取数据失败")
	}
	for _, v := range list {
        v.Compose()
    }
	return
}
// GetInfoById 通过id获取
func (s *codeRule) GetInfoById(ctx context.Context,id int) (info *model.CodeRule, err error) {
	if id == 0 {
		err = gerror.New("参数错误")
		return
	}
	err = dao.CodeRule.Ctx(ctx).Where(dao.CodeRule.Columns.Id, id).WithAll().Scan(&info)
	if err != nil {
		g.Log().Error(err)
	}
	if info == nil || err != nil {
		err = gerror.New("获取信息失败")
	}
	info.Compose()
	return
}
// Create 添加
func (s *codeRule) Create(ctx context.Context,req *dao.CodeRuleAddReq) (id int64, err error) {
	id, err = dao.CodeRule.Ctx(ctx).InsertAndGetId(req)
	return
}
// Add 添加
func (s *codeRule) Add(ctx context.Context,req *dao.CodeRuleAddReq) (err error) {
	_, err = dao.CodeRule.Ctx(ctx).Insert(req)
	return
}
// Edit 修改
func (s *codeRule) Edit(ctx context.Context,req *dao.CodeRuleEditReq) error {    
	_, err := dao.CodeRule.Ctx(ctx).FieldsEx(dao.CodeRule.Columns.Id).Where(dao.CodeRule.Columns.Id, req.Id).
		Update(req)
	return err
}
// DeleteByIds 删除
func (s *codeRule) DeleteByIds(ctx context.Context,ids []int) (err error) {
	if len(ids) == 0 {
		err = gerror.New("参数错误")
		return
	}	
	_, err = dao.CodeRule.Ctx(ctx).Delete(dao.CodeRule.Columns.Id+" in (?)", ids)
	if err != nil {
		g.Log().Error(err)
		err = gerror.New("删除失败")
	}
	return
}
