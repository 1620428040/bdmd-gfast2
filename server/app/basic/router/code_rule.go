// ==========================================================================
// GFast自动生成路由代码，只生成一次，按需修改,再次生成不会覆盖.
// 生成日期：2023-11-06 13:26:05
// 生成路径: gfast/app/basic/router/code_rule.go
// 生成人：guo
// ==========================================================================


package router
import (
    "gfast/app/basic/api"
    "gfast/middleware"
    "github.com/gogf/gf/frame/g"
    "github.com/gogf/gf/net/ghttp"    
    sysApi "gfast/app/system/api"    
)
//加载路由
func init() {
    s := g.Server()
    s.Group("/", func(group *ghttp.RouterGroup) {
        group.Group("/basic", func(group *ghttp.RouterGroup) {
            group.Group("/codeRule", func(group *ghttp.RouterGroup) {
                //gToken拦截器                
                sysApi.GfToken.AuthMiddleware(group)                
                //context拦截器
                group.Middleware(middleware.Ctx, middleware.Auth)                
                //后台操作日志记录
                group.Hook("/*", ghttp.HookAfterOutput, sysApi.SysOperLog.OperationLog)                
                group.GET("list", api.CodeRule.List)
                group.GET("get", api.CodeRule.Get)
                group.POST("create", api.CodeRule.Create)
                group.POST("add", api.CodeRule.Add)
                group.PUT("edit", api.CodeRule.Edit)
                group.DELETE("delete", api.CodeRule.Delete)                
            })
        })
    })
}
