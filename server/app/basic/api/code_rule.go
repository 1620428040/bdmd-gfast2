// ==========================================================================
// GFast自动生成控制器相关代码，只生成一次，按需修改,再次生成不会覆盖.
// 生成日期：2023-11-06 13:26:05
// 生成路径: gfast/app/basic/api/code_rule.go
// 生成人：guo
// ==========================================================================


package api
import (    
    sysApi "gfast/app/system/api"    
    "gfast/app/basic/dao"
    "gfast/app/basic/service"
    "github.com/gogf/gf/frame/g"
    "github.com/gogf/gf/net/ghttp"
    "github.com/gogf/gf/util/gvalid"    
)
type codeRule struct {    
    sysApi.SystemBase    
}
var CodeRule = new(codeRule)
// List 列表
func (c *codeRule) List(r *ghttp.Request) {
	var req *dao.CodeRuleSearchReq
	//获取参数
	if err := r.Parse(&req); err != nil {
		c.FailJsonExit(r, err.(gvalid.Error).FirstString())
	}
	req.Ctx = r.GetCtx()
	total, page, list, err := service.CodeRule.GetList(req)
	if err != nil {
		c.FailJsonExit(r, err.Error())
	}
	result := g.Map{
		"currentPage": page,
		"total":       total,
		"list":        list,
	}
	c.SusJsonExit(r, result)
}
// Create 添加
func (c *codeRule) Create(r *ghttp.Request) {
    var req *dao.CodeRuleAddReq
    //获取参数
    if err := r.Parse(&req); err != nil {
        c.FailJsonExit(r, err.(gvalid.Error).FirstString())
    }    
    id, err := service.CodeRule.Create(r.GetCtx(),req)
    if err != nil {
        c.FailJsonExit(r, err.Error())
    }
    c.SusJsonExit(r, g.Map{
        "id": id,
    })
}
// Add 添加
func (c *codeRule) Add(r *ghttp.Request) {
    var req *dao.CodeRuleAddReq
    //获取参数
    if err := r.Parse(&req); err != nil {
        c.FailJsonExit(r, err.(gvalid.Error).FirstString())
    }    
    err := service.CodeRule.Add(r.GetCtx(),req)
    if err != nil {
        c.FailJsonExit(r, err.Error())
    }
    c.SusJsonExit(r, "添加成功")
}
// Get 获取
func (c *codeRule) Get(r *ghttp.Request) {
	id := r.GetInt("id")
	info, err := service.CodeRule.GetInfoById(r.GetCtx(),id)
	if err != nil {
		c.FailJsonExit(r, err.Error())
	}
	c.SusJsonExit(r, info)
}
// Edit 修改
func (c *codeRule) Edit(r *ghttp.Request) {
    var req *dao.CodeRuleEditReq
    //获取参数
    if err := r.Parse(&req); err != nil {
        c.FailJsonExit(r, err.(gvalid.Error).FirstString())
    }    
    err := service.CodeRule.Edit(r.GetCtx(),req)
    if err != nil {
        c.FailJsonExit(r, err.Error())
    }
    c.SusJsonExit(r, "修改成功")
}
// Delete 删除
func (c *codeRule) Delete(r *ghttp.Request) {
	ids := r.GetInts("ids")
	err := service.CodeRule.DeleteByIds(r.GetCtx(),ids)
	if err != nil {
		c.FailJsonExit(r, err.Error())
	}
	c.SusJsonExit(r, "删除成功")
}
