// ==========================================================================
// GFast自动生成业务逻辑层相关代码，只生成一次，按需修改,再次生成不会覆盖.
// 生成日期：
// 生成路径: gfast/app/test/service/demo_gen.go
// 生成人：gfast
// ==========================================================================

package service

import (
	"context"
	comModel "gfast/app/common/model"
	"gfast/app/test/dao"
	"gfast/app/test/model"
	"github.com/gogf/gf/errors/gerror"
	"github.com/gogf/gf/frame/g"
)

type demoGen struct {
}

var DemoGen = new(demoGen)

// GetList 获取任务列表
func (s *demoGen) GetList(req *dao.DemoGenSearchReq) (total, page int, list []*model.DemoGen, err error) {
	m := dao.DemoGen.Ctx(req.Ctx)
	if req.DemoName != "" {
		m = m.Where(dao.DemoGen.Columns.DemoName+" like ?", "%"+req.DemoName+"%")
	}
	if req.DemoCate != "" {
		m = m.Where(dao.DemoGen.Columns.DemoCate+" = ?", req.DemoCate)
	}
	total, err = m.Count()
	if err != nil {
		g.Log().Error(err)
		err = gerror.New("获取总行数失败")
		return
	}
	if req.PageNum == 0 {
		req.PageNum = 1
	}
	page = req.PageNum
	if req.PageSize == 0 {
		req.PageSize = comModel.PageSize
	}
	order := "id asc"
	if req.OrderBy != "" {
		order = req.OrderBy
	}
	err = m.Page(page, req.PageSize).Order(order).Scan(&list)
	if err != nil {
		g.Log().Error(err)
		err = gerror.New("获取数据失败")
	}
	return
}

// GetInfoById 通过id获取
func (s *demoGen) GetInfoById(ctx context.Context, id uint) (info *model.DemoGen, err error) {
	if id == 0 {
		err = gerror.New("参数错误")
		return
	}
	err = dao.DemoGen.Ctx(ctx).Where(dao.DemoGen.Columns.Id, id).Scan(&info)
	if err != nil {
		g.Log().Error(err)
	}
	if info == nil || err != nil {
		err = gerror.New("获取信息失败")
	}
	return
}

// Add 添加
func (s *demoGen) Add(ctx context.Context, req *dao.DemoGenAddReq) (err error) {
	_, err = dao.DemoGen.Ctx(ctx).Insert(req)
	return
}

// Edit 修改
func (s *demoGen) Edit(ctx context.Context, req *dao.DemoGenEditReq) error {
	_, err := dao.DemoGen.Ctx(ctx).FieldsEx(dao.DemoGen.Columns.Id).Where(dao.DemoGen.Columns.Id, req.Id).
		Update(req)
	return err
}

// DeleteByIds 删除
func (s *demoGen) DeleteByIds(ctx context.Context, ids []int) (err error) {
	if len(ids) == 0 {
		err = gerror.New("参数错误")
		return
	}
	_, err = dao.DemoGen.Ctx(ctx).Delete(dao.DemoGen.Columns.Id+" in (?)", ids)
	if err != nil {
		g.Log().Error(err)
		err = gerror.New("删除失败")
	}
	return
}
