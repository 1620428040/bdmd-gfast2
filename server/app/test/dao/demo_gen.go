// ==========================================================================
// GFast自动生成dao操作代码，无需手动修改，重新生成不会自动覆盖.
// 生成日期：
// 生成路径: gfast/app/test/dao/demo_gen.go
// 生成人：gfast
// ==========================================================================

package dao

import (
	comModel "gfast/app/common/model"
	"gfast/app/test/dao/internal"
)

// demoGenDao is the manager for logic model data accessing and custom defined data operations functions management.
// You can define custom methods on it to extend its functionality as you wish.
type demoGenDao struct {
	*internal.DemoGenDao
}

var (
	// DemoGen is globally public accessible object for table tools_gen_table operations.
	DemoGen = demoGenDao{
		internal.NewDemoGenDao(),
	}
)

// Fill with you ideas below.

// DemoGenSearchReq 分页请求参数
type DemoGenSearchReq struct {
	DemoName string `p:"demoName"` //姓名
	DemoCate string `p:"demoCate"` //分类
	comModel.PageReq
}

// DemoGenAddReq 添加操作请求参数
type DemoGenAddReq struct {
	DemoName string `p:"demoName" v:"required#姓名不能为空"`
	DemoCate string `p:"demoCate" v:"required#分类不能为空"`
}

// DemoGenEditReq 修改操作请求参数
type DemoGenEditReq struct {
	Id       uint   `p:"id" v:"required#主键ID不能为空"`
	DemoName string `p:"demoName" v:"required#姓名不能为空"`
	DemoCate string `p:"demoCate" v:"required#分类不能为空"`
}
