// ==========================================================================
// GFast自动生成model代码，无需手动修改，重新生成会自动覆盖.
// 生成日期：
// 生成路径: gfast/app/test/model/demo_gen.go
// 生成人：gfast
// ==========================================================================

package model

// DemoGen is the golang structure for table demo_gen.
type DemoGen struct {
	Id       uint   `orm:"id,primary" json:"id"`      //
	DemoName string `orm:"demo_name" json:"demoName"` // 姓名
	DemoCate string `orm:"demo_cate" json:"demoCate"` // 分类
}
