// ==========================================================================
// GFast自动生成控制器相关代码，只生成一次，按需修改,再次生成不会覆盖.
// 生成日期：
// 生成路径: gfast/app/test/api/demo_gen.go
// 生成人：gfast
// ==========================================================================

package api

import (
	sysApi "gfast/app/system/api"
	"gfast/app/test/dao"
	"gfast/app/test/service"
	"github.com/gogf/gf/frame/g"
	"github.com/gogf/gf/net/ghttp"
	"github.com/gogf/gf/text/gstr"
	"github.com/gogf/gf/util/gvalid"
)

type demoGen struct {
	sysApi.SystemBase
}

var DemoGen = new(demoGen)

// List 列表
func (c *demoGen) List(r *ghttp.Request) {
	var req *dao.DemoGenSearchReq
	//获取参数
	if err := r.Parse(&req); err != nil {
		c.FailJsonExit(r, err.(gvalid.Error).FirstString())
	}
	req.Ctx = r.GetCtx()
	total, page, list, err := service.DemoGen.GetList(req)
	if err != nil {
		c.FailJsonExit(r, err.Error())
	}
	result := g.Map{
		"currentPage": page,
		"total":       total,
		"list":        list,
	}
	c.SusJsonExit(r, result)
}

// Add 添加
func (c *demoGen) Add(r *ghttp.Request) {
	var req *dao.DemoGenAddReq
	//获取参数
	if err := r.Parse(&req); err != nil {
		c.FailJsonExit(r, err.(gvalid.Error).FirstString())
	}
	demoCate := r.GetStrings("demoCate")
	if len(demoCate) > 0 {
		req.DemoCate = gstr.Join(demoCate, ",")
	} else {
		req.DemoCate = ""
	}
	err := service.DemoGen.Add(r.GetCtx(), req)
	if err != nil {
		c.FailJsonExit(r, err.Error())
	}
	c.SusJsonExit(r, "添加成功")
}

// Get 获取
func (c *demoGen) Get(r *ghttp.Request) {
	id := r.GetUint("id")
	info, err := service.DemoGen.GetInfoById(r.GetCtx(), id)
	if err != nil {
		c.FailJsonExit(r, err.Error())
	}
	c.SusJsonExit(r, info)
}

// Edit 修改
func (c *demoGen) Edit(r *ghttp.Request) {
	var req *dao.DemoGenEditReq
	//获取参数
	if err := r.Parse(&req); err != nil {
		c.FailJsonExit(r, err.(gvalid.Error).FirstString())
	}
	demoCate := r.GetStrings("demoCate")
	if len(demoCate) > 0 {
		req.DemoCate = gstr.Join(demoCate, ",")
	} else {
		req.DemoCate = ""
	}
	err := service.DemoGen.Edit(r.GetCtx(), req)
	if err != nil {
		c.FailJsonExit(r, err.Error())
	}
	c.SusJsonExit(r, "修改成功")
}

// Delete 删除
func (c *demoGen) Delete(r *ghttp.Request) {
	ids := r.GetInts("ids")
	err := service.DemoGen.DeleteByIds(r.GetCtx(), ids)
	if err != nil {
		c.FailJsonExit(r, err.Error())
	}
	c.SusJsonExit(r, "删除成功")
}
