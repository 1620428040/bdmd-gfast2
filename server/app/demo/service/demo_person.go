// ==========================================================================
// GFast自动生成业务逻辑层相关代码，只生成一次，按需修改,再次生成不会覆盖.
// 生成日期：2023-12-04 10:56:05
// 生成路径: gfast/app/demo/service/demo_person.go
// 生成人：guo
// ==========================================================================


package service
import (
    "context"
	comModel "gfast/app/common/model"
	"gfast/app/demo/dao"
	"gfast/app/demo/model"	
	"github.com/gogf/gf/errors/gerror"
	"github.com/gogf/gf/frame/g"
)
type demoPerson struct {
}
type IDemoPerson interface {
	GetList(req *dao.DemoPersonSearchReq) (total, page int, list []*model.DemoPerson, err error)
	GetInfoById(ctx context.Context,id int) (info *model.DemoPerson, err error)
	Create(ctx context.Context,req *dao.DemoPersonAddReq) (id int64, err error)
    Edit(ctx context.Context,req *dao.DemoPersonEditReq) error
    DeleteByIds(ctx context.Context,ids []int) (err error)
}
var DemoPerson IDemoPerson = new(demoPerson)
// GetList 获取任务列表
func (s *demoPerson) GetList(req *dao.DemoPersonSearchReq) (total, page int, list []*model.DemoPerson, err error) {
	m := dao.DemoPerson.Ctx(req.Ctx)
	total, err = m.Count()
	if err != nil {
		g.Log().Error(err)
		err = gerror.New("获取总行数失败")
		return
	}    
    if req.PageNum == 0 {
        req.PageNum = 1
    }
    page = req.PageNum
    if req.PageSize == 0 {
        req.PageSize = comModel.PageSize
    }
    order:= "id asc"
    if req.OrderBy!=""{
        order = req.OrderBy
    }
    err = m.Page(page, req.PageSize).Order(order).WithAll().Scan(&list)    
	if err != nil {
		g.Log().Error(err)
		err = gerror.New("获取数据失败")
	}
	for _, v := range list {
        v.Compose()
    }
	return
}
// GetInfoById 通过id获取
func (s *demoPerson) GetInfoById(ctx context.Context,id int) (info *model.DemoPerson, err error) {
	if id == 0 {
		err = gerror.New("参数错误")
		return
	}
	err = dao.DemoPerson.Ctx(ctx).Where(dao.DemoPerson.Columns.Id, id).WithAll().Scan(&info)
	if err != nil {
		g.Log().Error(err)
	}
	if info == nil || err != nil {
		err = gerror.New("获取信息失败")
	}
	info.Compose()
	return
}
// Create 添加
func (s *demoPerson) Create(ctx context.Context,req *dao.DemoPersonAddReq) (id int64, err error) {    
	return dao.DemoPerson.Ctx(ctx).InsertAndGetId(req)
}
// Edit 修改
func (s *demoPerson) Edit(ctx context.Context,req *dao.DemoPersonEditReq) error {    
	_, err := dao.DemoPerson.Ctx(ctx).FieldsEx(dao.DemoPerson.Columns.Id).Where(dao.DemoPerson.Columns.Id, req.Id).
		Update(req)
	return err
}
// DeleteByIds 删除
func (s *demoPerson) DeleteByIds(ctx context.Context,ids []int) (err error) {
	if len(ids) == 0 {
		err = gerror.New("参数错误")
		return
	}    
	_, err = dao.DemoPerson.Ctx(ctx).Delete(dao.DemoPerson.Columns.Id+" in (?)", ids)
	if err != nil {
		g.Log().Error(err)
		err = gerror.New("删除失败")
	}
	return
}
