// ==========================================================================
// GFast自动生成业务逻辑层相关代码，只生成一次，按需修改,再次生成不会覆盖.
// 生成日期：2023-12-04 10:56:05
// 生成路径: gfast/app/demo/service/demo_entity_relevant.go
// 生成人：guo
// ==========================================================================


package service
import (
    "context"
	comModel "gfast/app/common/model"
	"gfast/app/demo/dao"
	"gfast/app/demo/model"	
	"github.com/gogf/gf/errors/gerror"
	"github.com/gogf/gf/frame/g"
)
type demoEntityRelevant struct {
}
type IDemoEntityRelevant interface {
	GetList(req *dao.DemoEntityRelevantSearchReq) (total, page int, list []*model.DemoEntityRelevant, err error)
	GetInfoById(ctx context.Context,id int) (info *model.DemoEntityRelevant, err error)
	Create(ctx context.Context,req *dao.DemoEntityRelevantAddReq) (id int64, err error)
    Edit(ctx context.Context,req *dao.DemoEntityRelevantEditReq) error
    DeleteByIds(ctx context.Context,ids []int) (err error)
    MultiSelected(ctx context.Context, id int, ids []int) (err error)
}
var DemoEntityRelevant IDemoEntityRelevant = new(demoEntityRelevant)
// GetList 获取任务列表
func (s *demoEntityRelevant) GetList(req *dao.DemoEntityRelevantSearchReq) (total, page int, list []*model.DemoEntityRelevant, err error) {
	m := dao.DemoEntityRelevant.Ctx(req.Ctx)     
        if req.DemoEntityId != "" {
            m = m.Where(dao.DemoEntityRelevant.Columns.DemoEntityId+" = ?", req.DemoEntityId)
        }
	total, err = m.Count()
	if err != nil {
		g.Log().Error(err)
		err = gerror.New("获取总行数失败")
		return
	}    
    if req.PageNum == 0 {
        req.PageNum = 1
    }
    page = req.PageNum
    if req.PageSize == 0 {
        req.PageSize = comModel.PageSize
    }
    order:= "id asc"
    if req.OrderBy!=""{
        order = req.OrderBy
    }
    err = m.Page(page, req.PageSize).Order(order).WithAll().Scan(&list)    
	if err != nil {
		g.Log().Error(err)
		err = gerror.New("获取数据失败")
	}
	for _, v := range list {
        v.Compose()
    }
	return
}
// GetInfoById 通过id获取
func (s *demoEntityRelevant) GetInfoById(ctx context.Context,id int) (info *model.DemoEntityRelevant, err error) {
	if id == 0 {
		err = gerror.New("参数错误")
		return
	}
	err = dao.DemoEntityRelevant.Ctx(ctx).Where(dao.DemoEntityRelevant.Columns.Id, id).WithAll().Scan(&info)
	if err != nil {
		g.Log().Error(err)
	}
	if info == nil || err != nil {
		err = gerror.New("获取信息失败")
	}
	info.Compose()
	return
}
// Create 添加
func (s *demoEntityRelevant) Create(ctx context.Context,req *dao.DemoEntityRelevantAddReq) (id int64, err error) {    
	return dao.DemoEntityRelevant.Ctx(ctx).InsertAndGetId(req)
}
// Edit 修改
func (s *demoEntityRelevant) Edit(ctx context.Context,req *dao.DemoEntityRelevantEditReq) error {    
	_, err := dao.DemoEntityRelevant.Ctx(ctx).FieldsEx(dao.DemoEntityRelevant.Columns.Id).Where(dao.DemoEntityRelevant.Columns.Id, req.Id).
		Update(req)
	return err
}
// DeleteByIds 删除
func (s *demoEntityRelevant) DeleteByIds(ctx context.Context,ids []int) (err error) {
	if len(ids) == 0 {
		err = gerror.New("参数错误")
		return
	}    
	_, err = dao.DemoEntityRelevant.Ctx(ctx).Delete(dao.DemoEntityRelevant.Columns.Id+" in (?)", ids)
	if err != nil {
		g.Log().Error(err)
		err = gerror.New("删除失败")
	}
	return
}
func (s *demoEntityRelevant) MultiSelected(ctx context.Context, id int, ids []int) (err error) {
	list := make([]*model.DemoEntityRelevant, 0)
	for _, _id := range ids {
		list = append(list, &model.DemoEntityRelevant{
			DemoEntityId:  id,
			RelevantId: _id,
		})
	}
	_, err = dao.DemoEntityRelevant.Ctx(ctx).Insert(list)
	return
}
