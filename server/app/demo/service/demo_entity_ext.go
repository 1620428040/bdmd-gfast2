// ==========================================================================
// GFast自动生成业务逻辑层相关代码，只生成一次，按需修改,再次生成不会覆盖.
// 生成日期：2023-12-04 10:56:05
// 生成路径: gfast/app/demo/service/demo_entity_ext.go
// 生成人：guo
// ==========================================================================


package service
import (
    "context"
	comModel "gfast/app/common/model"
	"gfast/app/demo/dao"
	"gfast/app/demo/model"	
	"github.com/gogf/gf/errors/gerror"
	"github.com/gogf/gf/frame/g"
)
type demoEntityExt struct {
}
type IDemoEntityExt interface {
	GetList(req *dao.DemoEntityExtSearchReq) (total, page int, list []*model.DemoEntityExt, err error)
	GetInfoById(ctx context.Context,id int) (info *model.DemoEntityExt, err error)
	Create(ctx context.Context,req *dao.DemoEntityExtAddReq) (id int64, err error)
    Edit(ctx context.Context,req *dao.DemoEntityExtEditReq) error
    DeleteByIds(ctx context.Context,ids []int) (err error)
}
var DemoEntityExt IDemoEntityExt = new(demoEntityExt)
// GetList 获取任务列表
func (s *demoEntityExt) GetList(req *dao.DemoEntityExtSearchReq) (total, page int, list []*model.DemoEntityExt, err error) {
	m := dao.DemoEntityExt.Ctx(req.Ctx)
	total, err = m.Count()
	if err != nil {
		g.Log().Error(err)
		err = gerror.New("获取总行数失败")
		return
	}    
    if req.PageNum == 0 {
        req.PageNum = 1
    }
    page = req.PageNum
    if req.PageSize == 0 {
        req.PageSize = comModel.PageSize
    }
    order:= "id asc"
    if req.OrderBy!=""{
        order = req.OrderBy
    }
    err = m.Page(page, req.PageSize).Order(order).WithAll().Scan(&list)    
	if err != nil {
		g.Log().Error(err)
		err = gerror.New("获取数据失败")
	}
	for _, v := range list {
        v.Compose()
    }
	return
}
// GetInfoById 通过id获取
func (s *demoEntityExt) GetInfoById(ctx context.Context,id int) (info *model.DemoEntityExt, err error) {
	if id == 0 {
		err = gerror.New("参数错误")
		return
	}
	err = dao.DemoEntityExt.Ctx(ctx).Where(dao.DemoEntityExt.Columns.Id, id).WithAll().Scan(&info)
	if err != nil {
		g.Log().Error(err)
	}
	if info == nil || err != nil {
		err = gerror.New("获取信息失败")
	}
	info.Compose()
	return
}
// Create 添加
func (s *demoEntityExt) Create(ctx context.Context,req *dao.DemoEntityExtAddReq) (id int64, err error) {    
    {
        id, err = DemoEntity.Create(ctx, &dao.DemoEntityAddReq{            
            Title:     req.Title,            
            CreatorId:     req.CreatorId,            
            ProtoId:     req.ProtoId,            
        })
        if err != nil {
            return
        }
        req.EntityId = int(id)
    }    
	return dao.DemoEntityExt.Ctx(ctx).InsertAndGetId(req)
}
// Edit 修改
func (s *demoEntityExt) Edit(ctx context.Context,req *dao.DemoEntityExtEditReq) error {    
    {
        err := DemoEntity.Edit(ctx, &dao.DemoEntityEditReq{
            Id:        req.EntityId,            
            Title:     req.Title,            
            CreatorId:     req.CreatorId,            
            ProtoId:     req.ProtoId,            
        })
        if err != nil {
            return err
        }
    }    
	_, err := dao.DemoEntityExt.Ctx(ctx).FieldsEx(dao.DemoEntityExt.Columns.Id).Where(dao.DemoEntityExt.Columns.Id, req.Id).
		Update(req)
	return err
}
// DeleteByIds 删除
func (s *demoEntityExt) DeleteByIds(ctx context.Context,ids []int) (err error) {
	if len(ids) == 0 {
		err = gerror.New("参数错误")
		return
	}    
	{
        list := make([]*model.DemoEntityExt, 0)
        err = dao.DemoEntityExt.Ctx(ctx).WhereIn(dao.DemoEntityExt.Columns.Id, ids).Scan(&list)
        if err != nil {
            return
        }
        ids2 := make([]int, 0)
        for _, rec := range list {
            ids2 = append(ids2, rec.EntityId)
        }
        err = DemoEntity.DeleteByIds(ctx, ids2)
        if err != nil {
            return
        }
    }    
	_, err = dao.DemoEntityExt.Ctx(ctx).Delete(dao.DemoEntityExt.Columns.Id+" in (?)", ids)
	if err != nil {
		g.Log().Error(err)
		err = gerror.New("删除失败")
	}
	return
}
