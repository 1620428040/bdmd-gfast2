// ==========================================================================
// GFast自动生成业务逻辑层相关代码，只生成一次，按需修改,再次生成不会覆盖.
// 生成日期：2023-12-06 20:41:23
// 生成路径: gfast/app/demo/service/demo_simple.go
// 生成人：guo
// ==========================================================================


package service
import (
    "context"
	comModel "gfast/app/common/model"
	"gfast/app/demo/dao"
	"gfast/app/demo/model"	
	"github.com/gogf/gf/errors/gerror"
	"github.com/gogf/gf/frame/g"
)
type demoSimple struct {
}
type IDemoSimple interface {
	GetList(req *dao.DemoSimpleSearchReq) (total, page int, list []*model.DemoSimple, err error)
	GetInfoById(ctx context.Context,id int) (info *model.DemoSimple, err error)
	Create(ctx context.Context,req *dao.DemoSimpleAddReq) (id int64, err error)
    Edit(ctx context.Context,req *dao.DemoSimpleEditReq) error
    DeleteByIds(ctx context.Context,ids []int) (err error)
    ChangeEnable(ctx context.Context,req *dao.DemoSimpleEnableReq) error
}
var DemoSimple IDemoSimple = new(demoSimple)
// GetList 获取任务列表
func (s *demoSimple) GetList(req *dao.DemoSimpleSearchReq) (total, page int, list []*model.DemoSimple, err error) {
	m := dao.DemoSimple.Ctx(req.Ctx)
	total, err = m.Count()
	if err != nil {
		g.Log().Error(err)
		err = gerror.New("获取总行数失败")
		return
	}    
    if req.PageNum == 0 {
        req.PageNum = 1
    }
    page = req.PageNum
    if req.PageSize == 0 {
        req.PageSize = comModel.PageSize
    }
    order:= "id asc"
    if req.OrderBy!=""{
        order = req.OrderBy
    }
    err = m.Page(page, req.PageSize).Order(order).WithAll().Scan(&list)    
	if err != nil {
		g.Log().Error(err)
		err = gerror.New("获取数据失败")
	}
	for _, v := range list {
        v.Compose()
    }
	return
}
// GetInfoById 通过id获取
func (s *demoSimple) GetInfoById(ctx context.Context,id int) (info *model.DemoSimple, err error) {
	if id == 0 {
		err = gerror.New("参数错误")
		return
	}
	err = dao.DemoSimple.Ctx(ctx).Where(dao.DemoSimple.Columns.Id, id).WithAll().Scan(&info)
	if err != nil {
		g.Log().Error(err)
	}
	if info == nil || err != nil {
		err = gerror.New("获取信息失败")
	}
	info.Compose()
	return
}
// Create 添加
func (s *demoSimple) Create(ctx context.Context,req *dao.DemoSimpleAddReq) (id int64, err error) {    
	return dao.DemoSimple.Ctx(ctx).InsertAndGetId(req)
}
// Edit 修改
func (s *demoSimple) Edit(ctx context.Context,req *dao.DemoSimpleEditReq) error {    
	_, err := dao.DemoSimple.Ctx(ctx).FieldsEx(dao.DemoSimple.Columns.Id).Where(dao.DemoSimple.Columns.Id, req.Id).
		Update(req)
	return err
}
// DeleteByIds 删除
func (s *demoSimple) DeleteByIds(ctx context.Context,ids []int) (err error) {
	if len(ids) == 0 {
		err = gerror.New("参数错误")
		return
	}    
	_, err = dao.DemoSimple.Ctx(ctx).Delete(dao.DemoSimple.Columns.Id+" in (?)", ids)
	if err != nil {
		g.Log().Error(err)
		err = gerror.New("删除失败")
	}
	return
}
// ChangeEnable 修改状态
func (s *demoSimple) ChangeEnable(ctx context.Context,req *dao.DemoSimpleEnableReq) error {
	_, err := dao.DemoSimple.Ctx(ctx).WherePri(req.Id).Update(g.Map{
		dao.DemoSimple.Columns.Enable: req.Enable,
	})
	return err
}
