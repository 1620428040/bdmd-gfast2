// ==========================================================================
// GFast自动生成dao操作代码，无需手动修改，重新生成不会自动覆盖.
// 生成日期：2023-12-04 10:56:05
// 生成路径: gfast/app/demo/dao/m_demo_record.go
// 生成人：guo
// ==========================================================================


package dao
import (
    comModel "gfast/app/common/model"
    "gfast/app/demo/dao/internal"    
)
// demoRecordDao is the manager for logic model data accessing and custom defined data operations functions management.
// You can define custom methods on it to extend its functionality as you wish.
type demoRecordDao struct {
	*internal.DemoRecordDao
}
var (
    // DemoRecord is globally public accessible object for table tools_gen_table operations.
    DemoRecord = demoRecordDao{
        internal.NewDemoRecordDao(),
    }
)


// Fill with you ideas below.


// DemoRecordSearchReq 分页请求参数
type DemoRecordSearchReq struct {    
    comModel.PageReq
}
// DemoRecordAddReq 添加操作请求参数
type DemoRecordAddReq struct {    
    EntityId  int   `p:"entityId" `    
    Desc  string   `p:"desc" `    
}
// DemoRecordEditReq 修改操作请求参数
type DemoRecordEditReq struct {
    Id    int  `p:"id" v:"required#主键ID不能为空"`    
    EntityId  int `p:"entityId" `    
    Desc  string `p:"desc" `    
}
