// ==========================================================================
// GFast自动生成dao操作代码，无需手动修改，重新生成不会自动覆盖.
// 生成日期：2023-12-04 10:56:05
// 生成路径: gfast/app/demo/dao/m_demo_entity.go
// 生成人：guo
// ==========================================================================


package dao
import (
    comModel "gfast/app/common/model"
    "gfast/app/demo/dao/internal"    
)
// demoEntityDao is the manager for logic model data accessing and custom defined data operations functions management.
// You can define custom methods on it to extend its functionality as you wish.
type demoEntityDao struct {
	*internal.DemoEntityDao
}
var (
    // DemoEntity is globally public accessible object for table tools_gen_table operations.
    DemoEntity = demoEntityDao{
        internal.NewDemoEntityDao(),
    }
)


// Fill with you ideas below.


// DemoEntitySearchReq 分页请求参数
type DemoEntitySearchReq struct {    
    comModel.PageReq
}
// DemoEntityAddReq 添加操作请求参数
type DemoEntityAddReq struct {    
    Title  string   `p:"title" `    
    CreatorId  int   `p:"creatorId" `    
    ProtoId  int   `p:"protoId" `    
}
// DemoEntityEditReq 修改操作请求参数
type DemoEntityEditReq struct {
    Id    int  `p:"id" v:"required#主键ID不能为空"`    
    Title  string `p:"title" `    
    CreatorId  int `p:"creatorId" `    
    ProtoId  int `p:"protoId" `    
}
