// ==========================================================================
// GFast自动生成dao操作代码，无需手动修改，重新生成不会自动覆盖.
// 生成日期：2023-12-06 20:41:23
// 生成路径: gfast/app/demo/dao/m_demo_simple.go
// 生成人：guo
// ==========================================================================


package dao
import (
    comModel "gfast/app/common/model"
    "gfast/app/demo/dao/internal"    
    "github.com/gogf/gf/os/gtime"    
)
// demoSimpleDao is the manager for logic model data accessing and custom defined data operations functions management.
// You can define custom methods on it to extend its functionality as you wish.
type demoSimpleDao struct {
	*internal.DemoSimpleDao
}
var (
    // DemoSimple is globally public accessible object for table tools_gen_table operations.
    DemoSimple = demoSimpleDao{
        internal.NewDemoSimpleDao(),
    }
)


// Fill with you ideas below.


// DemoSimpleSearchReq 分页请求参数
type DemoSimpleSearchReq struct {    
    comModel.PageReq
}
// DemoSimpleAddReq 添加操作请求参数
type DemoSimpleAddReq struct {    
    Name  string   `p:"name" `    
    Num  int   `p:"num" `    
    CreateAt  *gtime.Time   `p:"createAt" `    
    Image  string   `p:"image" `    
    Desc  string   `p:"desc" `    
    Desc2  string   `p:"desc2" `    
    Enable  int   `p:"enable" `    
}
// DemoSimpleEditReq 修改操作请求参数
type DemoSimpleEditReq struct {
    Id    int  `p:"id" v:"required#主键ID不能为空"`    
    Name  string `p:"name" `    
    Num  int `p:"num" `    
    CreateAt  *gtime.Time `p:"createAt" `    
    Image  string `p:"image" `    
    Desc  string `p:"desc" `    
    Desc2  string `p:"desc2" `    
    Enable  int `p:"enable" `    
}
// DemoSimpleEnableReq 设置用户状态参数
type DemoSimpleEnableReq struct {
	Id    int  `p:"id" v:"required#主键ID不能为空"`
	Enable int   `p:"enable" v:"required#启用不能为空"`
}
