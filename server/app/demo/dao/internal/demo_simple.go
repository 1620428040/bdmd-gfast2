// ==========================================================================
// GFast自动生成dao internal操作代码，无需手动修改，重新生成会自动覆盖.
// 生成日期：2023-12-06 20:41:23
// 生成路径: gfast/app/demo/dao/internal/m_demo_simple.go
// 生成人：guo
// ==========================================================================


package internal
import (
    "context"
    "github.com/gogf/gf/database/gdb"
    "github.com/gogf/gf/frame/g"
)
// DemoSimpleDao is the manager for logic model data accessing and custom defined data operations functions management.
type DemoSimpleDao struct {
    Table   string         // Table is the underlying table name of the DAO.
    Group   string         // Group is the database configuration group name of current DAO.
    Columns DemoSimpleColumns // Columns is the short type for Columns, which contains all the column names of Table for convenient usage.
}
// DemoSimpleColumns defines and stores column names for table m_demo_simple.
type DemoSimpleColumns struct {    
    Id  string  // id    
    Name  string  // 名称    
    Num  string  // 编号    
    CreateAt  string  // 创建时间    
    Image  string  // 截图    
    Desc  string  // 描述    
    Desc2  string  // 描述2    
    Enable  string  // 启用    
}
var demoSimpleColumns = DemoSimpleColumns{    
    Id:  "id",    
    Name:  "name",    
    Num:  "num",    
    CreateAt:  "create_at",    
    Image:  "image",    
    Desc:  "desc",    
    Desc2:  "desc2",    
    Enable:  "enable",    
}
// NewDemoSimpleDao creates and returns a new DAO object for table data access.
func NewDemoSimpleDao() *DemoSimpleDao {
	return &DemoSimpleDao{
        Group:    "default",
        Table: "m_demo_simple",
        Columns:demoSimpleColumns,
	}
}
// DB retrieves and returns the underlying raw database management object of current DAO.
func (dao *DemoSimpleDao) DB() gdb.DB {
	return g.DB(dao.Group)
}
// Ctx creates and returns the Model for current DAO, It automatically sets the context for current operation.
func (dao *DemoSimpleDao) Ctx(ctx context.Context) *gdb.Model {
	return dao.DB().Model(dao.Table).Safe().Ctx(ctx)
}
// Transaction wraps the transaction logic using function f.
// It rollbacks the transaction and returns the error from function f if it returns non-nil error.
// It commits the transaction and returns nil if function f returns nil.
//
// Note that, you should not Commit or Rollback the transaction in function f
// as it is automatically handled by this function.
func (dao *DemoSimpleDao) Transaction(ctx context.Context, f func(ctx context.Context, tx *gdb.TX) error) (err error) {
	return dao.Ctx(ctx).Transaction(ctx, f)
}