// ==========================================================================
// GFast自动生成dao internal操作代码，无需手动修改，重新生成会自动覆盖.
// 生成日期：2023-12-04 14:31:41
// 生成路径: gfast/app/demo/dao/internal/m_demo_person.go
// 生成人：guo
// ==========================================================================


package internal
import (
    "context"
    "github.com/gogf/gf/database/gdb"
    "github.com/gogf/gf/frame/g"
)
// DemoPersonDao is the manager for logic model data accessing and custom defined data operations functions management.
type DemoPersonDao struct {
    Table   string         // Table is the underlying table name of the DAO.
    Group   string         // Group is the database configuration group name of current DAO.
    Columns DemoPersonColumns // Columns is the short type for Columns, which contains all the column names of Table for convenient usage.
}
// DemoPersonColumns defines and stores column names for table m_demo_person.
type DemoPersonColumns struct {    
    Id  string  // id    
    Name  string  // 姓名    
    Value1  string  // 值1    
    Value2  string  // 值2    
}
var demoPersonColumns = DemoPersonColumns{    
    Id:  "id",    
    Name:  "name",    
    Value1:  "value1",    
    Value2:  "value2",    
}
// NewDemoPersonDao creates and returns a new DAO object for table data access.
func NewDemoPersonDao() *DemoPersonDao {
	return &DemoPersonDao{
        Group:    "default",
        Table: "m_demo_person",
        Columns:demoPersonColumns,
	}
}
// DB retrieves and returns the underlying raw database management object of current DAO.
func (dao *DemoPersonDao) DB() gdb.DB {
	return g.DB(dao.Group)
}
// Ctx creates and returns the Model for current DAO, It automatically sets the context for current operation.
func (dao *DemoPersonDao) Ctx(ctx context.Context) *gdb.Model {
	return dao.DB().Model(dao.Table).Safe().Ctx(ctx)
}
// Transaction wraps the transaction logic using function f.
// It rollbacks the transaction and returns the error from function f if it returns non-nil error.
// It commits the transaction and returns nil if function f returns nil.
//
// Note that, you should not Commit or Rollback the transaction in function f
// as it is automatically handled by this function.
func (dao *DemoPersonDao) Transaction(ctx context.Context, f func(ctx context.Context, tx *gdb.TX) error) (err error) {
	return dao.Ctx(ctx).Transaction(ctx, f)
}