// ==========================================================================
// GFast自动生成dao internal操作代码，无需手动修改，重新生成会自动覆盖.
// 生成日期：2023-12-04 14:31:41
// 生成路径: gfast/app/demo/dao/internal/m_demo_record.go
// 生成人：guo
// ==========================================================================


package internal
import (
    "context"
    "github.com/gogf/gf/database/gdb"
    "github.com/gogf/gf/frame/g"
)
// DemoRecordDao is the manager for logic model data accessing and custom defined data operations functions management.
type DemoRecordDao struct {
    Table   string         // Table is the underlying table name of the DAO.
    Group   string         // Group is the database configuration group name of current DAO.
    Columns DemoRecordColumns // Columns is the short type for Columns, which contains all the column names of Table for convenient usage.
}
// DemoRecordColumns defines and stores column names for table m_demo_record.
type DemoRecordColumns struct {    
    Id  string  // id    
    EntityId  string  // 实体    
    Desc  string  // 说明    
}
var demoRecordColumns = DemoRecordColumns{    
    Id:  "id",    
    EntityId:  "entity_id",    
    Desc:  "desc",    
}
// NewDemoRecordDao creates and returns a new DAO object for table data access.
func NewDemoRecordDao() *DemoRecordDao {
	return &DemoRecordDao{
        Group:    "default",
        Table: "m_demo_record",
        Columns:demoRecordColumns,
	}
}
// DB retrieves and returns the underlying raw database management object of current DAO.
func (dao *DemoRecordDao) DB() gdb.DB {
	return g.DB(dao.Group)
}
// Ctx creates and returns the Model for current DAO, It automatically sets the context for current operation.
func (dao *DemoRecordDao) Ctx(ctx context.Context) *gdb.Model {
	return dao.DB().Model(dao.Table).Safe().Ctx(ctx)
}
// Transaction wraps the transaction logic using function f.
// It rollbacks the transaction and returns the error from function f if it returns non-nil error.
// It commits the transaction and returns nil if function f returns nil.
//
// Note that, you should not Commit or Rollback the transaction in function f
// as it is automatically handled by this function.
func (dao *DemoRecordDao) Transaction(ctx context.Context, f func(ctx context.Context, tx *gdb.TX) error) (err error) {
	return dao.Ctx(ctx).Transaction(ctx, f)
}