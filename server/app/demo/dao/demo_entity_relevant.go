// ==========================================================================
// GFast自动生成dao操作代码，无需手动修改，重新生成不会自动覆盖.
// 生成日期：2023-12-04 10:56:05
// 生成路径: gfast/app/demo/dao/m_demo_entity_relevant.go
// 生成人：guo
// ==========================================================================


package dao
import (
    comModel "gfast/app/common/model"
    "gfast/app/demo/dao/internal"    
)
// demoEntityRelevantDao is the manager for logic model data accessing and custom defined data operations functions management.
// You can define custom methods on it to extend its functionality as you wish.
type demoEntityRelevantDao struct {
	*internal.DemoEntityRelevantDao
}
var (
    // DemoEntityRelevant is globally public accessible object for table tools_gen_table operations.
    DemoEntityRelevant = demoEntityRelevantDao{
        internal.NewDemoEntityRelevantDao(),
    }
)


// Fill with you ideas below.


// DemoEntityRelevantSearchReq 分页请求参数
type DemoEntityRelevantSearchReq struct {    
    DemoEntityId  string `p:"demoEntityId"` //实体    
    comModel.PageReq
}
// DemoEntityRelevantAddReq 添加操作请求参数
type DemoEntityRelevantAddReq struct {    
    DemoEntityId  int   `p:"demoEntityId" `    
    RelevantId  int   `p:"relevantId" `    
}
// DemoEntityRelevantEditReq 修改操作请求参数
type DemoEntityRelevantEditReq struct {
    Id    int  `p:"id" v:"required#主键ID不能为空"`    
    DemoEntityId  int `p:"demoEntityId" `    
    RelevantId  int `p:"relevantId" `    
}
