// ==========================================================================
// GFast自动生成dao操作代码，无需手动修改，重新生成不会自动覆盖.
// 生成日期：2023-12-04 10:56:05
// 生成路径: gfast/app/demo/dao/m_demo_entity_ext.go
// 生成人：guo
// ==========================================================================


package dao
import (
    comModel "gfast/app/common/model"
    "gfast/app/demo/dao/internal"    
)
// demoEntityExtDao is the manager for logic model data accessing and custom defined data operations functions management.
// You can define custom methods on it to extend its functionality as you wish.
type demoEntityExtDao struct {
	*internal.DemoEntityExtDao
}
var (
    // DemoEntityExt is globally public accessible object for table tools_gen_table operations.
    DemoEntityExt = demoEntityExtDao{
        internal.NewDemoEntityExtDao(),
    }
)


// Fill with you ideas below.


// DemoEntityExtSearchReq 分页请求参数
type DemoEntityExtSearchReq struct {    
    comModel.PageReq
}
// DemoEntityExtAddReq 添加操作请求参数
type DemoEntityExtAddReq struct {    
    EntityId  int   `p:"entityId" `    
    Title  string   `p:"title" `    
    CreatorId  int   `p:"creatorId" `    
    ProtoId  int   `p:"protoId" `    
    Value3  string   `p:"value3" `    
}
// DemoEntityExtEditReq 修改操作请求参数
type DemoEntityExtEditReq struct {
    Id    int  `p:"id" v:"required#主键ID不能为空"`    
    EntityId  int `p:"entityId" `    
    Title  string `p:"title" `    
    CreatorId  int `p:"creatorId" `    
    ProtoId  int `p:"protoId" `    
    Value3  string `p:"value3" `    
}
