// ==========================================================================
// GFast自动生成dao操作代码，无需手动修改，重新生成不会自动覆盖.
// 生成日期：2023-12-04 10:56:05
// 生成路径: gfast/app/demo/dao/m_demo_person.go
// 生成人：guo
// ==========================================================================


package dao
import (
    comModel "gfast/app/common/model"
    "gfast/app/demo/dao/internal"    
)
// demoPersonDao is the manager for logic model data accessing and custom defined data operations functions management.
// You can define custom methods on it to extend its functionality as you wish.
type demoPersonDao struct {
	*internal.DemoPersonDao
}
var (
    // DemoPerson is globally public accessible object for table tools_gen_table operations.
    DemoPerson = demoPersonDao{
        internal.NewDemoPersonDao(),
    }
)


// Fill with you ideas below.


// DemoPersonSearchReq 分页请求参数
type DemoPersonSearchReq struct {    
    comModel.PageReq
}
// DemoPersonAddReq 添加操作请求参数
type DemoPersonAddReq struct {    
    Name  string   `p:"name" `    
    Value1  string   `p:"value1" `    
    Value2  string   `p:"value2" `    
}
// DemoPersonEditReq 修改操作请求参数
type DemoPersonEditReq struct {
    Id    int  `p:"id" v:"required#主键ID不能为空"`    
    Name  string `p:"name" `    
    Value1  string `p:"value1" `    
    Value2  string `p:"value2" `    
}
