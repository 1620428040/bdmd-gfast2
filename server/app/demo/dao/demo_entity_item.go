// ==========================================================================
// GFast自动生成dao操作代码，无需手动修改，重新生成不会自动覆盖.
// 生成日期：2023-12-04 10:56:05
// 生成路径: gfast/app/demo/dao/m_demo_entity_item.go
// 生成人：guo
// ==========================================================================


package dao
import (
    comModel "gfast/app/common/model"
    "gfast/app/demo/dao/internal"    
)
// demoEntityItemDao is the manager for logic model data accessing and custom defined data operations functions management.
// You can define custom methods on it to extend its functionality as you wish.
type demoEntityItemDao struct {
	*internal.DemoEntityItemDao
}
var (
    // DemoEntityItem is globally public accessible object for table tools_gen_table operations.
    DemoEntityItem = demoEntityItemDao{
        internal.NewDemoEntityItemDao(),
    }
)


// Fill with you ideas below.


// DemoEntityItemSearchReq 分页请求参数
type DemoEntityItemSearchReq struct {    
    EntityId  string `p:"entityId"` //entity_id    
    comModel.PageReq
}
// DemoEntityItemAddReq 添加操作请求参数
type DemoEntityItemAddReq struct {    
    Name  string   `p:"name" `    
    EntityId  int   `p:"entityId" `    
}
// DemoEntityItemEditReq 修改操作请求参数
type DemoEntityItemEditReq struct {
    Id    int  `p:"id" v:"required#主键ID不能为空"`    
    Name  string `p:"name" `    
    EntityId  int `p:"entityId" `    
}
