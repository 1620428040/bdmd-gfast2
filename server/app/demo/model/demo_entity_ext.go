// ==========================================================================
// 自动生成代码，无需手动修改，重新生成会自动覆盖.
// 数据:parse.Model
// ==========================================================================
package model

import "github.com/gogf/gf/util/gmeta"






type DemoEntityExt struct {
    gmeta.Meta `orm:"table:m_demo_entity_ext"`
    // id integer id
    Id int `orm:"id,primary" json:"id"`
    // entity extend 实体
    EntityId int `orm:"entity_id" json:"entityId"`
    *DemoEntity  `orm:"with:id=entity_id"`
    // value3 string 值3
    Value3 string `orm:"value3" json:"value3"`
}

func (v *DemoEntityExt) Compose() {
    if v.DemoEntity != nil {
        v.DemoEntity.Compose()
    }
}
