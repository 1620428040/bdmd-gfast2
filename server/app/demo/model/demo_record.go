// ==========================================================================
// 自动生成代码，无需手动修改，重新生成会自动覆盖.
// 数据:parse.Model
// ==========================================================================
package model

import "github.com/gogf/gf/util/gmeta"






type DemoRecord struct {
    gmeta.Meta `orm:"table:m_demo_record"`
    // id integer id
    Id int `orm:"id,primary" json:"id"`
    // entity refer 实体
    EntityId int `orm:"entity_id" json:"entityId"`
    EntityTitle string `json:"entityTitle"`
    Entity *DemoEntity `orm:"with:id=entity_id" json:"entity"`
    // desc text 说明
    Desc string `orm:"desc" json:"desc"`
}

func (v *DemoRecord) Compose() {
    if v.Entity != nil {
        v.Entity.Compose()
        v.EntityTitle = v.Entity.Title
    }
}
