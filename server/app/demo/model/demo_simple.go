// ==========================================================================
// 自动生成代码，无需手动修改，重新生成会自动覆盖.
// 数据:parse.Model
// ==========================================================================
package model

import "github.com/gogf/gf/util/gmeta"
import "github.com/gogf/gf/os/gtime"






type DemoSimple struct {
    gmeta.Meta `orm:"table:m_demo_simple"`
    // id integer id
    Id int `orm:"id,primary" json:"id"`
    // name string 名称
    Name string `orm:"name" json:"name"`
    // num integer 编号
    Num int `orm:"num" json:"num"`
    // create_at datetime 创建时间
    CreateAt *gtime.Time `orm:"create_at" json:"createAt"`
    // image image 截图
    Image string `orm:"image" json:"image"`
    // desc text 描述
    Desc string `orm:"desc" json:"desc"`
    // desc2 richtext 描述2
    Desc2 string `orm:"desc2" json:"desc2"`
    // enable enable 启用
    Enable int `orm:"enable" json:"enable"`
}

func (v *DemoSimple) Compose() {
}
