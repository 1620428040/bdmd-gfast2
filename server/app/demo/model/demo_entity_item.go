// ==========================================================================
// 自动生成代码，无需手动修改，重新生成会自动覆盖.
// 数据:parse.Model
// ==========================================================================
package model

import "github.com/gogf/gf/util/gmeta"






type DemoEntityItem struct {
    gmeta.Meta `orm:"table:m_demo_entity_item"`
    // id integer id
    Id int `orm:"id,primary" json:"id"`
    // name string 名称
    Name string `orm:"name" json:"name"`
    // entity_id integer entity_id
    EntityId int `orm:"entity_id" json:"entityId"`
}

func (v *DemoEntityItem) Compose() {
}
