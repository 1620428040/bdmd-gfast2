// ==========================================================================
// 自动生成代码，无需手动修改，重新生成会自动覆盖.
// 数据:parse.Model
// ==========================================================================
package model

import "github.com/gogf/gf/util/gmeta"






type DemoEntity struct {
    gmeta.Meta `orm:"table:m_demo_entity"`
    // id integer id
    Id int `orm:"id,primary" json:"id"`
    // title string 标题
    Title string `orm:"title" json:"title"`
    // creator refer 创建者
    CreatorId int `orm:"creator_id" json:"creatorId"`
    CreatorName string `json:"creatorName"`
    Creator *DemoPerson `orm:"with:id=creator_id" json:"creator"`
    // proto refer 原型
    ProtoId int `orm:"proto_id" json:"protoId"`
    ProtoTitle string `json:"protoTitle"`
    Proto *ProtoOfDemoEntity  `orm:"with:id=proto_id" json:"proto"`
    // items annex 明细
    Items []*DemoEntityItem `orm:"with:entity_id=id" json:"items"`
    // relevant multi 相关人员
    Relevant []*DemoEntityRelevant `orm:"with:demo_entity_id=id" json:"relevant"`
}

func (v *DemoEntity) Compose() {
    if v.Creator != nil {
        v.Creator.Compose()
        v.CreatorName = v.Creator.Name
    }
    if v.Proto != nil {
        v.Proto.Compose()
        v.ProtoTitle = v.Proto.Title
    }
    for _, elem := range v.Items {
        elem.Compose()
    }
    for _, elem := range v.Relevant {
        elem.Compose()
    }
}




type ProtoOfDemoEntity struct {
    gmeta.Meta `orm:"table:m_demo_entity"`
    // id integer id
    Id int `orm:"id,primary" json:"id"`
    // title string 标题
    Title string `orm:"title" json:"title"`
}

func (v *ProtoOfDemoEntity) Compose() {
}
