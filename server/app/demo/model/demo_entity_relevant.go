// ==========================================================================
// 自动生成代码，无需手动修改，重新生成会自动覆盖.
// 数据:parse.Model
// ==========================================================================
package model

import "github.com/gogf/gf/util/gmeta"






type DemoEntityRelevant struct {
    gmeta.Meta `orm:"table:m_demo_entity_relevant"`
    // id integer ID
    Id int `orm:"id,primary" json:"id"`
    // demo_entity integer 实体
    DemoEntityId int `orm:"demo_entity_id" json:"demoEntityId"`
    // relevant refer 人物
    RelevantId int `orm:"relevant_id" json:"relevantId"`
    RelevantName string `json:"relevantName"`
    Relevant *DemoPerson `orm:"with:id=relevant_id" json:"relevant"`
}

func (v *DemoEntityRelevant) Compose() {
    if v.Relevant != nil {
        v.Relevant.Compose()
        v.RelevantName = v.Relevant.Name
    }
}
