// ==========================================================================
// 自动生成代码，无需手动修改，重新生成会自动覆盖.
// 数据:parse.Model
// ==========================================================================
package model

import "github.com/gogf/gf/util/gmeta"






type DemoPerson struct {
    gmeta.Meta `orm:"table:m_demo_person"`
    // id integer id
    Id int `orm:"id,primary" json:"id"`
    // name string 姓名
    Name string `orm:"name" json:"name"`
    // value1 string 值1
    Value1 string `orm:"value1" json:"value1"`
    // value2 string 值2
    Value2 string `orm:"value2" json:"value2"`
}

func (v *DemoPerson) Compose() {
}
