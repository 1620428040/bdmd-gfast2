// ==========================================================================
// GFast自动生成控制器相关代码，只生成一次，按需修改,再次生成不会覆盖.
// 生成日期：2023-12-04 10:56:05
// 生成路径: gfast/app/demo/api/demo_entity_ext.go
// 生成人：guo
// ==========================================================================


package api
import (    
    sysApi "gfast/app/system/api"    
    "gfast/app/demo/dao"
    "gfast/app/demo/service"
    "github.com/gogf/gf/frame/g"
    "github.com/gogf/gf/net/ghttp"
    "github.com/gogf/gf/util/gvalid"    
)
type demoEntityExt struct {    
    sysApi.SystemBase    
}
var DemoEntityExt = new(demoEntityExt)
// List 列表
func (c *demoEntityExt) List(r *ghttp.Request) {
	var req *dao.DemoEntityExtSearchReq
	//获取参数
	if err := r.Parse(&req); err != nil {
		c.FailJsonExit(r, err.(gvalid.Error).FirstString())
	}
	req.Ctx = r.GetCtx()
	total, page, list, err := service.DemoEntityExt.GetList(req)
	if err != nil {
		c.FailJsonExit(r, err.Error())
	}
	result := g.Map{
		"currentPage": page,
		"total":       total,
		"list":        list,
	}
	c.SusJsonExit(r, result)
}
// Create 添加
func (c *demoEntityExt) Create(r *ghttp.Request) {
    var req *dao.DemoEntityExtAddReq
    //获取参数
    if err := r.Parse(&req); err != nil {
        c.FailJsonExit(r, err.(gvalid.Error).FirstString())
    }    
    id, err := service.DemoEntityExt.Create(r.GetCtx(),req)
    if err != nil {
        c.FailJsonExit(r, err.Error())
    }
    c.SusJsonExit(r, g.Map{
        "id": id,
    })
}
// Get 获取
func (c *demoEntityExt) Get(r *ghttp.Request) {
	id := r.GetInt("id")
	info, err := service.DemoEntityExt.GetInfoById(r.GetCtx(),id)
	if err != nil {
		c.FailJsonExit(r, err.Error())
	}
	c.SusJsonExit(r, info)
}
// Edit 修改
func (c *demoEntityExt) Edit(r *ghttp.Request) {
    var req *dao.DemoEntityExtEditReq
    //获取参数
    if err := r.Parse(&req); err != nil {
        c.FailJsonExit(r, err.(gvalid.Error).FirstString())
    }    
    err := service.DemoEntityExt.Edit(r.GetCtx(),req)
    if err != nil {
        c.FailJsonExit(r, err.Error())
    }
    c.SusJsonExit(r, "修改成功")
}
// Delete 删除
func (c *demoEntityExt) Delete(r *ghttp.Request) {
	ids := r.GetInts("ids")
	err := service.DemoEntityExt.DeleteByIds(r.GetCtx(),ids)
	if err != nil {
		c.FailJsonExit(r, err.Error())
	}
	c.SusJsonExit(r, "删除成功")
}
