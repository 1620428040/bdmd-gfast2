// ==========================================================================
// GFast自动生成控制器相关代码，只生成一次，按需修改,再次生成不会覆盖.
// 生成日期：2023-12-06 20:41:23
// 生成路径: gfast/app/demo/api/demo_simple.go
// 生成人：guo
// ==========================================================================


package api
import (    
    sysApi "gfast/app/system/api"    
    "gfast/app/demo/dao"
    "gfast/app/demo/service"
    "github.com/gogf/gf/frame/g"
    "github.com/gogf/gf/net/ghttp"
    "github.com/gogf/gf/util/gvalid"    
)
type demoSimple struct {    
    sysApi.SystemBase    
}
var DemoSimple = new(demoSimple)
// List 列表
func (c *demoSimple) List(r *ghttp.Request) {
	var req *dao.DemoSimpleSearchReq
	//获取参数
	if err := r.Parse(&req); err != nil {
		c.FailJsonExit(r, err.(gvalid.Error).FirstString())
	}
	req.Ctx = r.GetCtx()
	total, page, list, err := service.DemoSimple.GetList(req)
	if err != nil {
		c.FailJsonExit(r, err.Error())
	}
	result := g.Map{
		"currentPage": page,
		"total":       total,
		"list":        list,
	}
	c.SusJsonExit(r, result)
}
// Create 添加
func (c *demoSimple) Create(r *ghttp.Request) {
    var req *dao.DemoSimpleAddReq
    //获取参数
    if err := r.Parse(&req); err != nil {
        c.FailJsonExit(r, err.(gvalid.Error).FirstString())
    }    
    id, err := service.DemoSimple.Create(r.GetCtx(),req)
    if err != nil {
        c.FailJsonExit(r, err.Error())
    }
    c.SusJsonExit(r, g.Map{
        "id": id,
    })
}
// Get 获取
func (c *demoSimple) Get(r *ghttp.Request) {
	id := r.GetInt("id")
	info, err := service.DemoSimple.GetInfoById(r.GetCtx(),id)
	if err != nil {
		c.FailJsonExit(r, err.Error())
	}
	c.SusJsonExit(r, info)
}
// Edit 修改
func (c *demoSimple) Edit(r *ghttp.Request) {
    var req *dao.DemoSimpleEditReq
    //获取参数
    if err := r.Parse(&req); err != nil {
        c.FailJsonExit(r, err.(gvalid.Error).FirstString())
    }    
    err := service.DemoSimple.Edit(r.GetCtx(),req)
    if err != nil {
        c.FailJsonExit(r, err.Error())
    }
    c.SusJsonExit(r, "修改成功")
}
// Delete 删除
func (c *demoSimple) Delete(r *ghttp.Request) {
	ids := r.GetInts("ids")
	err := service.DemoSimple.DeleteByIds(r.GetCtx(),ids)
	if err != nil {
		c.FailJsonExit(r, err.Error())
	}
	c.SusJsonExit(r, "删除成功")
}
// ChangeEnable 修改状态
func (c *demoSimple) ChangeEnable(r *ghttp.Request){
	   var req *dao.DemoSimpleEnableReq
	   //获取参数
	   if err := r.Parse(&req); err != nil {
	       c.FailJsonExit(r, err.(gvalid.Error).FirstString())
	   }
	   if err := service.DemoSimple.ChangeEnable(r.GetCtx(),req); err != nil {
	       c.FailJsonExit(r, err.Error())
	   } else {
	       c.SusJsonExit(r, "状态设置成功")
	   }
}
