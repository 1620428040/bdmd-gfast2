/*
code_rule
  id  number
  key  input
  name  input
  prefix  input
  time  input
  serial_begin  number
  serial_length  number
  serial_counter  number
*/
import request from '@/utils/request'
// 查询编码规则列表
export function listCodeRule(query) {
  return request({
    url: '/basic/codeRule/list',
    method: 'get',
    params: query
  })
}
// 查询编码规则详细
export function getCodeRule(id) {
  return request({
    url: '/basic/codeRule/get',
    method: 'get',
    params: {
     id: id.toString()
    }
  })
}
// 新增编码规则并返回id
export function createCodeRule(data) {
  return request({
    url: '/basic/codeRule/create',
    method: 'post',
    data: data
  })
}
// 新增编码规则
export function addCodeRule(data) {
  return request({
    url: '/basic/codeRule/add',
    method: 'post',
    data: data
  })
}
// 修改编码规则
export function updateCodeRule(data) {
  return request({
    url: '/basic/codeRule/edit',
    method: 'put',
    data: data
  })
}
// 删除编码规则
export function delCodeRule(ids) {
  return request({
    url: '/basic/codeRule/delete',
    method: 'delete',
    data:{
       ids:ids
    }
  })
}
