/*
dept
  dept_id  number
  dept_name  input
  parent_id_dept_id  treeselect  sys_dept
*/
import request from '@/utils/request'
// 查询部门(sys)列表
export function listDept(query) {
  return request({
    url: '/shim/dept/list',
    method: 'get',
    params: query
  })
}
// 查询部门(sys)详细
export function getDept(deptId) {
  return request({
    url: '/shim/dept/get',
    method: 'get',
    params: {
     id: deptId.toString()
    }
  })
}
// 新增部门(sys)并返回id
export function createDept(data) {
  return request({
    url: '/shim/dept/create',
    method: 'post',
    data: data
  })
}
// 修改部门(sys)
export function updateDept(data) {
  return request({
    url: '/shim/dept/edit',
    method: 'put',
    data: data
  })
}
// 删除部门(sys)
export function delDept(deptIds) {
  return request({
    url: '/shim/dept/delete',
    method: 'delete',
    data:{
       ids:deptIds
    }
  })
}
// 关联sys_dept表选项
export function listParentIdDeptId(query){
   return request({
     url: '/shim/dept/list',
     method: 'get',
     params: query
   })
}
