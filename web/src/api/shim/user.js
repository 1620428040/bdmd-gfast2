/*
user
  id  number
  user_nickname  input
*/
import request from '@/utils/request'
// 查询用户(sys)列表
export function listUser(query) {
  return request({
    url: '/shim/user/list',
    method: 'get',
    params: query
  })
}
// 查询用户(sys)详细
export function getUser(id) {
  return request({
    url: '/shim/user/get',
    method: 'get',
    params: {
     id: id.toString()
    }
  })
}
// 新增用户(sys)并返回id
export function createUser(data) {
  return request({
    url: '/shim/user/create',
    method: 'post',
    data: data
  })
}
// 修改用户(sys)
export function updateUser(data) {
  return request({
    url: '/shim/user/edit',
    method: 'put',
    data: data
  })
}
// 删除用户(sys)
export function delUser(ids) {
  return request({
    url: '/shim/user/delete',
    method: 'delete',
    data:{
       ids:ids
    }
  })
}
