/*
demo_entity_item
  id  number
  name  input
  entity_id  underlying
*/
import request from '@/utils/request'
// 查询明细列表
export function listDemoEntityItem(query) {
  return request({
    url: '/demo/demoEntityItem/list',
    method: 'get',
    params: query
  })
}
// 查询明细详细
export function getDemoEntityItem(id) {
  return request({
    url: '/demo/demoEntityItem/get',
    method: 'get',
    params: {
     id: id.toString()
    }
  })
}
// 新增明细并返回id
export function createDemoEntityItem(data) {
  return request({
    url: '/demo/demoEntityItem/create',
    method: 'post',
    data: data
  })
}
// 修改明细
export function updateDemoEntityItem(data) {
  return request({
    url: '/demo/demoEntityItem/edit',
    method: 'put',
    data: data
  })
}
// 删除明细
export function delDemoEntityItem(ids) {
  return request({
    url: '/demo/demoEntityItem/delete',
    method: 'delete',
    data:{
       ids:ids
    }
  })
}
