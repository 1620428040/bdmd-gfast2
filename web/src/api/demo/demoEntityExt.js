/*
demo_entity_ext
  id  number
  entity_id  extend  m_demo_entity
  title  input
  creator_id  refer  m_demo_person
  proto_id  refer  m_demo_entity
  value3  input
*/
import request from '@/utils/request'
// 查询实体2列表
export function listDemoEntityExt(query) {
  return request({
    url: '/demo/demoEntityExt/list',
    method: 'get',
    params: query
  })
}
// 查询实体2详细
export function getDemoEntityExt(id) {
  return request({
    url: '/demo/demoEntityExt/get',
    method: 'get',
    params: {
     id: id.toString()
    }
  })
}
// 新增实体2并返回id
export function createDemoEntityExt(data) {
  return request({
    url: '/demo/demoEntityExt/create',
    method: 'post',
    data: data
  })
}
// 修改实体2
export function updateDemoEntityExt(data) {
  return request({
    url: '/demo/demoEntityExt/edit',
    method: 'put',
    data: data
  })
}
// 删除实体2
export function delDemoEntityExt(ids) {
  return request({
    url: '/demo/demoEntityExt/delete',
    method: 'delete',
    data:{
       ids:ids
    }
  })
}
// 关联m_demo_entity表选项
export function listEntityId(query){
   return request({
     url: '/demo/demoEntity/list',
     method: 'get',
     params: query
   })
}
// 关联m_demo_person表选项
export function listCreatorId(query){
   return request({
     url: '/demo/demoPerson/list',
     method: 'get',
     params: query
   })
}
// 关联m_demo_entity表选项
export function listProtoId(query){
   return request({
     url: '/demo/demoEntity/list',
     method: 'get',
     params: query
   })
}
