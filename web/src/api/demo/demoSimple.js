/*
demo_simple
  id  number
  name  input
  num  number
  create_at  datetime
  image  imagefile
  desc  textarea
  desc2  richtext
  enable  enable
*/
import request from '@/utils/request'
// 查询简单模型列表
export function listDemoSimple(query) {
  return request({
    url: '/demo/demoSimple/list',
    method: 'get',
    params: query
  })
}
// 查询简单模型详细
export function getDemoSimple(id) {
  return request({
    url: '/demo/demoSimple/get',
    method: 'get',
    params: {
     id: id.toString()
    }
  })
}
// 新增简单模型并返回id
export function createDemoSimple(data) {
  return request({
    url: '/demo/demoSimple/create',
    method: 'post',
    data: data
  })
}
// 修改简单模型
export function updateDemoSimple(data) {
  return request({
    url: '/demo/demoSimple/edit',
    method: 'put',
    data: data
  })
}
// 删除简单模型
export function delDemoSimple(ids) {
  return request({
    url: '/demo/demoSimple/delete',
    method: 'delete',
    data:{
       ids:ids
    }
  })
}
// 简单模型启用修改
export function changeDemoSimpleEnable(id,enable) {
  const data = {
    id,
    enable
  }
  return request({
    url: '/demo/demoSimple/changeEnable',
    method: 'put',
    data:data
  })
}
