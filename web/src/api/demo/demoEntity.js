/*
demo_entity
  id  number
  title  input
  creator_id  refer  m_demo_person
  proto_id  refer  m_demo_entity
*/
import request from '@/utils/request'
// 查询实体列表
export function listDemoEntity(query) {
  return request({
    url: '/demo/demoEntity/list',
    method: 'get',
    params: query
  })
}
// 查询实体详细
export function getDemoEntity(id) {
  return request({
    url: '/demo/demoEntity/get',
    method: 'get',
    params: {
     id: id.toString()
    }
  })
}
// 新增实体并返回id
export function createDemoEntity(data) {
  return request({
    url: '/demo/demoEntity/create',
    method: 'post',
    data: data
  })
}
// 修改实体
export function updateDemoEntity(data) {
  return request({
    url: '/demo/demoEntity/edit',
    method: 'put',
    data: data
  })
}
// 删除实体
export function delDemoEntity(ids) {
  return request({
    url: '/demo/demoEntity/delete',
    method: 'delete',
    data:{
       ids:ids
    }
  })
}
// 关联m_demo_person表选项
export function listCreatorId(query){
   return request({
     url: '/demo/demoPerson/list',
     method: 'get',
     params: query
   })
}
// 关联m_demo_entity表选项
export function listProtoId(query){
   return request({
     url: '/demo/demoEntity/list',
     method: 'get',
     params: query
   })
}
