/*
demo_entity_relevant
  id  number
  demo_entity_id  underlying
  relevant_id  refer  m_demo_person
*/
import request from '@/utils/request'
// 查询人物列表
export function listDemoEntityRelevant(query) {
  return request({
    url: '/demo/demoEntityRelevant/list',
    method: 'get',
    params: query
  })
}
// 查询人物详细
export function getDemoEntityRelevant(id) {
  return request({
    url: '/demo/demoEntityRelevant/get',
    method: 'get',
    params: {
     id: id.toString()
    }
  })
}
// 新增人物并返回id
export function createDemoEntityRelevant(data) {
  return request({
    url: '/demo/demoEntityRelevant/create',
    method: 'post',
    data: data
  })
}
// 修改人物
export function updateDemoEntityRelevant(data) {
  return request({
    url: '/demo/demoEntityRelevant/edit',
    method: 'put',
    data: data
  })
}
// 删除人物
export function delDemoEntityRelevant(ids) {
  return request({
    url: '/demo/demoEntityRelevant/delete',
    method: 'delete',
    data:{
       ids:ids
    }
  })
}
// 关联m_demo_person表选项
export function listRelevantId(query){
   return request({
     url: '/demo/demoPerson/list',
     method: 'get',
     params: query
   })
}
export function selectDemoEntityRelevant(id, list){
    return request({
        url: '/demo/demoEntityRelevant/selected',
        method: 'patch',
        params: {
            "id": id,
            "list": list
        }
    })
}
