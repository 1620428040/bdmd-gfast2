/*
demo_person
  id  number
  name  input
  value1  input
  value2  input
*/
import request from '@/utils/request'
// 查询人物列表
export function listDemoPerson(query) {
  return request({
    url: '/demo/demoPerson/list',
    method: 'get',
    params: query
  })
}
// 查询人物详细
export function getDemoPerson(id) {
  return request({
    url: '/demo/demoPerson/get',
    method: 'get',
    params: {
     id: id.toString()
    }
  })
}
// 新增人物并返回id
export function createDemoPerson(data) {
  return request({
    url: '/demo/demoPerson/create',
    method: 'post',
    data: data
  })
}
// 修改人物
export function updateDemoPerson(data) {
  return request({
    url: '/demo/demoPerson/edit',
    method: 'put',
    data: data
  })
}
// 删除人物
export function delDemoPerson(ids) {
  return request({
    url: '/demo/demoPerson/delete',
    method: 'delete',
    data:{
       ids:ids
    }
  })
}
