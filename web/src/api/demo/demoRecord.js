/*
demo_record
  id  number
  entity_id  refer  m_demo_entity
  desc  textarea
*/
import request from '@/utils/request'
// 查询记录列表
export function listDemoRecord(query) {
  return request({
    url: '/demo/demoRecord/list',
    method: 'get',
    params: query
  })
}
// 查询记录详细
export function getDemoRecord(id) {
  return request({
    url: '/demo/demoRecord/get',
    method: 'get',
    params: {
     id: id.toString()
    }
  })
}
// 新增记录并返回id
export function createDemoRecord(data) {
  return request({
    url: '/demo/demoRecord/create',
    method: 'post',
    data: data
  })
}
// 修改记录
export function updateDemoRecord(data) {
  return request({
    url: '/demo/demoRecord/edit',
    method: 'put',
    data: data
  })
}
// 删除记录
export function delDemoRecord(ids) {
  return request({
    url: '/demo/demoRecord/delete',
    method: 'delete',
    data:{
       ids:ids
    }
  })
}
// 关联m_demo_entity表选项
export function listEntityId(query){
   return request({
     url: '/demo/demoEntity/list',
     method: 'get',
     params: query
   })
}
