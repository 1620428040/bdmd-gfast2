import request from '@/utils/request'

// 生成编码
export function genCode(key){
    return request({
      url: '/code/gen',
      method: "post",
      data:{
        key: key
      }
    })
  }