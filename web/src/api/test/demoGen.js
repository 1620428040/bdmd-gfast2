import request from '@/utils/request'
// 查询代码生成测试列表
export function listDemoGen(query) {
  return request({
    url: '/test/demoGen/list',
    method: 'get',
    params: query
  })
}
// 查询代码生成测试详细
export function getDemoGen(id) {
  return request({
    url: '/test/demoGen/get',
    method: 'get',
    params: {
     id: id.toString()
    }
  })
}
// 新增代码生成测试
export function addDemoGen(data) {
  return request({
    url: '/test/demoGen/add',
    method: 'post',
    data: data
  })
}
// 修改代码生成测试
export function updateDemoGen(data) {
  return request({
    url: '/test/demoGen/edit',
    method: 'put',
    data: data
  })
}
// 删除代码生成测试
export function delDemoGen(ids) {
  return request({
    url: '/test/demoGen/delete',
    method: 'delete',
    data:{
       ids:ids
    }
  })
}
